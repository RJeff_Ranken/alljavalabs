import java.util.Scanner;

public class PlantingGrapevines
{
	public static void main(String[] args)
	{
		double V;
		double R;
		double E;
		double S;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Length of the row (in feet): ");
		R = keyboard.nextDouble();
		keyboard.nextLine();
		
		System.out.print("Space used by an end-post assembly (in feet): ");
		E = keyboard.nextDouble();
		keyboard.nextLine();
		
		System.out.print("Space between the vines(in feet): ");
		S = keyboard.nextDouble();
		keyboard.nextLine();
		
	    V = (R - (2*E)) / S;
		
		
		System.out.print("Number of grapevines that will fit in the row: " + V);
	}
}