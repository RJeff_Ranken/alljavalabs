import java.util.Scanner;

public class StockTransactionProgram
{
	public static void main(String[] args)
	{
		double perShare = 32.87;
		double commission = 0.02;
		double sharesOfStock = 1000;
		
		double perShare2Weeks = 33.92;
		double commission2Weeks = 0.02;
		double sharesOfStock2Weeks = 1000;
		
		double amountPaidForStocks = perShare * sharesOfStock;
		
		double amountOfCommission = amountPaidForStocks * commission;
		
		double totalAmountPaid = amountPaidForStocks + amountOfCommission;
		
		System.out.printf("Price of total stock: $%,.2f\n" 
						+"Commission: $%,.2f\n"
						+"totalAmountPaid: $%,.2f\n", amountPaidForStocks, amountOfCommission, totalAmountPaid);
						
		double amountSoldForStocks = perShare2Weeks * sharesOfStock2Weeks;

		double amountOfCommissionAfterSale = amountSoldForStocks * commission2Weeks;
		
		double totalAmountSold = amountSoldForStocks + amountOfCommissionAfterSale;
						
		System.out.printf("\nPrice of total stock sold: $%,.2f\n" 
						+"Commission after sales: $%,.2f\n", amountSoldForStocks, amountOfCommissionAfterSale);
						
		double bothCommissions = amountOfCommission + amountOfCommissionAfterSale;

		double profitAfter = totalAmountSold - bothCommissions;
						
		System.out.printf("\nProfit made: $%,.2f\n"
						+"\n ", profitAfter);
		
	}
}