package lab1;

public class ProductionWorker extends Employee{

	private int shift;
	private double payRate;
	public final int DAY_SHIFT = 1;
	public final int NIGHT_SHIFT = 2;
	
	public ProductionWorker(String n, String num, String date,
							int sh, double rate)
	{
		shift = sh;
		payRate = rate;
	}
	
	public ProductionWorker()
	{
		
	}

	public void setShift(int s) {
		shift = s;
	}

	public void setPayRate(double p) {
		payRate = p;
	}
	
	public int getShift() {
		return shift;
	}

	public double getPayRate() {
		return payRate;
	}
	
	public String toString()
	{
		String str;
		
		str = String.format(shift + "\n" + payRate);
		
		return str;
	}
}
