package lab1;

import java.util.Scanner;

public class V_Stock {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String nameInput;
		String numInput;
		String dateInput;
		int shiftInput;
		double rateInput;
		int dayNight;
		
		String empNameInput;
		String empNumInput;
		String hireInput;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter name of worker ");
		nameInput = keyboard.nextLine();
		System.out.print("Enter worker's ID ");
		numInput = keyboard.nextLine();
		System.out.print("Enter date ");
		dateInput = keyboard.nextLine();
		System.out.print("Enter the number of hours worked");
		shiftInput = keyboard.nextInt();
		System.out.print("Enter worker's pay rate ");
		rateInput = keyboard.nextDouble();
		System.out.print("What shift did the worker take");
		dayNight = keyboard.nextInt();
		
		ProductionWorker worker1 = new ProductionWorker(nameInput, numInput, dateInput, shiftInput, rateInput);
		
		String workerInfo = "Name: " + nameInput.concat("\nID: " + numInput).concat("\n" + dateInput);
		System.out.print(workerInfo + worker1.toString());
	}

}
