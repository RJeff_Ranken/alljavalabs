package lab4;

import java.util.Scanner;

public class Grades {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double grammerInput, spellingInput, lengthInput, contentInput;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter points for grammer: ");
		grammerInput = keyboard.nextDouble();
		
		System.out.print("Enter points for spelling: ");
		spellingInput = keyboard.nextDouble();
		
		System.out.print("Enter points for length: ");
		lengthInput = keyboard.nextDouble();
		
		System.out.print("Enter points for content: ");
		contentInput = keyboard.nextDouble();
		
		Essay firstEssay = new Essay();
		
		GradedActivity essay = new GradedActivity();
		
		firstEssay.setScore(grammerInput, spellingInput, lengthInput, contentInput);
		
		double total = firstEssay.getTotal();
		essay.setScore(total);
		
		System.out.print(total + " which is a " + essay.getGrade());
	}

}
