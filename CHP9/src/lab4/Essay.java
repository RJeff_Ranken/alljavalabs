package lab4;

import lab5.GradedActivity;

public class Essay extends GradedActivity{

	private double grammer;
	private double spelling;
	private double correctLength;
	private double content;
	
	public void setScore(double gr, double sp, double len, double cnt)
	{
		grammer = gr;
		spelling = sp;
		correctLength = len;
		content = cnt;
	}

	public double getGrammer() {
		return grammer;
	}

	public double getSpelling() {
		return spelling;
	}

	public double getCorrectLength() {
		return correctLength;
	}

	public double getContent() {
		return content;
	}

	public void setGrammer(double grammer) {
		this.grammer = grammer;
	}

	public void setSpelling(double spelling) {
		this.spelling = spelling;
	}

	public void setCorrectLength(double correctLength) {
		this.correctLength = correctLength;
	}

	public void setContent(double content) {
		this.content = content;
	}
	
	public double getTotal()
	{
		double total = 0;
		
		total = grammer + spelling + correctLength + content;
		
		return total;
	}
}
