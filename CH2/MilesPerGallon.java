import java.util.Scanner;

public class MilesPerGallon
{
	public static void main(String[] args)
	{  
		//variables 1st
		double milesDriven;
		double gasUsed;
		
		
		//scanner next
		Scanner keyboard = new Scanner(System.in);
		
		//println is after
		System.out.print("Enter the amount of miles that was driven: ");
		
		//keyboard is last
		milesDriven = keyboard.nextDouble();
		
		keyboard.nextLine();
		
		System.out.print("Enter the amount of gas that was used: ");
		
		gasUsed = keyboard.nextDouble();
		
		keyboard.nextLine();
		
		double MPG = milesDriven / gasUsed;
		
		System.out.print("The total MPG is " + (int)MPG + ".");
	}
}