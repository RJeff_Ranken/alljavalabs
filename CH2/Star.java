public class Star
{
	public static void main(String[] args)
	{
		String part1 = "   *   ";
		String part2 = "  ***  ";
		String part3 = " ***** ";
		String part4 = "*******";
		String part5 = " ***** ";
		String part6 = "  ***  ";
		String part7 = "   *   ";
		
		System.out.println(part1 + "\n"
						 + part2 + "\n"
						 + part3 + "\n"
						 + part4 + "\n"
						 + part5 + "\n"
						 + part6 + "\n"
						 + part7);
	}
}