public class LandCalculation
{
	public static void main(String[] args)
	{
		double oneAcre = 43560;
		double currentLand = 389767;
		
		double totalSQFT = currentLand / oneAcre;
		
		System.out.printf("Number of acres owned: %,.2f square feet", totalSQFT);
	}
}