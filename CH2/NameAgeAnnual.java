public class NameAgeAnnual
{
	public static void main(String[] args)
	{
		String name = "Richardo Jefferson";
		int age = 27;
		double annualPay = 98765.58;
		System.out.println("My name is " + name
							+ ", my age is " + age
							+ " and \nI hope to earn $" + annualPay
							+ " per year.");
	}
}