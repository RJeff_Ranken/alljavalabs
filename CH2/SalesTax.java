import java.util.Scanner;

public class SalesTax
{
	public static void main(String[] args)
	{
		double countySalesTax = 0.02;
		double stateSalesTax = 0.055;
		double totalSalesTax = countySalesTax + stateSalesTax;
		double userInput;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter the amount of your purchase: ");
		
		userInput = keyboard.nextDouble();
		
		keyboard.nextLine();
		
		double saleTotal = userInput + totalSalesTax;
		
		System.out.printf("Price of purchase: $%,.2f\n" 
						+"State sales tax: $%,.2f\n"
						+"County sales tax: $%,.2f\n"
						+"Total Sales tax: $%,.2f\n"
						+"Total of sale: $%,.2f", userInput, stateSalesTax, countySalesTax, totalSalesTax, saleTotal);
	}
}