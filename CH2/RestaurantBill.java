import java.util.Scanner;

public class RestaurantBill
{
	public static void main(String[] args)
	{
		double tip = 0.18;
		double tax = 0.075;
		double mealCharge;
		double taxAmount;
		double tipAmount;
		double totalBill;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter the amount of the meal: ");
		
		mealCharge = keyboard.nextDouble();
		
		keyboard.nextLine();
		
		taxAmount = mealCharge * tax;
		
		tipAmount = mealCharge * tip;
		
		totalBill = mealCharge + taxAmount + tipAmount;
		
		System.out.printf("Meal Charge: $%,.2f\n" 
						+"Tax: $%,.2f\n"
						+"Tip: $%,.2f\n"
						+"Total: $%,.2f\n", mealCharge, taxAmount, tipAmount, totalBill);
	}
}