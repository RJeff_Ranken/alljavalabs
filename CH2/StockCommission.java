import java.util.Scanner;

public class StockCommission
{
	public static void main(String[] args)
	{
		double perShare = 25.50;
		double commission = 0.02;
		double sharesOfStock = 1000;
		
		double amountPaidForStocks = perShare * sharesOfStock;
		
		double amountOfCommission = amountPaidForStocks * commission;
		
		double totalAmountPaid = amountPaidForStocks + amountOfCommission;
		
		System.out.printf("Price of total stock: $%,.2f\n" 
						+"Commission: $%,.2f\n"
						+"Total amount paid: $%,.2f\n", amountPaidForStocks, amountOfCommission, totalAmountPaid);
		
	}
}