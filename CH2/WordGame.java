import java.util.Scanner;

public class WordGame
{
	public static void main(String[] args)
	{
		String name;
		int age;
		String city;
		String college;
		String profession;
		String typeOfAnimal;
		String nameOfPet;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter the person's name: ");
		name = keyboard.nextLine();
		keyboard.nextLine();
		
		System.out.print("Enter the person's age: ");
		age = keyboard.nextInt();
		keyboard.nextLine();
		
		System.out.print("Enter a city name: ");
		city = keyboard.nextLine();
		keyboard.nextLine();
		
		System.out.print("Enter a college name: ");
		college = keyboard.nextLine();
		keyboard.nextLine();
		
		System.out.print("Enter the person's profession: ");
		profession = keyboard.nextLine();
		keyboard.nextLine();
		
		System.out.print("Enter a type of animal: ");
		typeOfAnimal = keyboard.nextLine();
		keyboard.nextLine();
		
		System.out.print("Enter the name of the animal: ");
		nameOfPet = keyboard.nextLine();
		keyboard.nextLine();
		
		System.out.print("There once was a person named " + name + " who lived in " + city + ". At the age of " + age + ",\n" +
						  name + " went to college at " + college + ". " + name + " graduated and went back to work as a" + "\n" +
						  profession + ". Then, " + name + " adopted a(n) " + typeOfAnimal + " named " + nameOfPet + ". They both lived\n" +
						  "happily ever after!");
	}
}