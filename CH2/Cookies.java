import java.util.Scanner;

public class Cookies
{
	public static void main(String[] args)
	{
		int bag = 40;
		int servings = 10;
		int caloriesPerServing = 300;
		int numOfCookiesPerServing = 4;
		int caloriePerCookie = 75;
		
		
		int userInput;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter the amount of cookies you have consumed: ");
		
		userInput = keyboard.nextInt();
		
		keyboard.nextLine();
		
		int perServing = userInput * caloriePerCookie;
		
		System.out.print(userInput + " cookies equals " + perServing + " calories.");
	}
}