import java.util.Scanner;

public class StringManipulator
{
	public static void main (String[] args)
	{
		String cityInput;
		int cityNameLength;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter a city name: ");
		cityInput = keyboard.nextLine();
		keyboard.nextLine();
		
		
		String upperCity = cityInput.toUpperCase();
		String lowerCity = cityInput.toLowerCase();
		char letter = cityInput.charAt(0);
		cityNameLength = cityInput.length();
		
		System.out.println(cityInput);
		System.out.println(upperCity);
		System.out.println(lowerCity);
		System.out.println(letter);
		System.out.println(cityNameLength);
	}
}