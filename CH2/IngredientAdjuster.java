import java.util.Scanner;

public class IngredientAdjuster
{
	public static void main (String[] args)
	{		
		double cupsOfSugar = 1.5;
		double cupOfButter = 1;
		double cupsOfFlour = 2.75;
		double batches;
		int userInput;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("How many cookies do you want to make?: ");
		
		userInput = keyboard.nextInt();
		
		batches = userInput / 48.0;
		
		double totalSugar = batches * cupsOfSugar;
		
		double totalButter = batches * cupOfButter;
		
		double totalFlour = batches * cupsOfFlour;
		
		System.out.printf("You want " + userInput + " cookies.\n"
					   + "You need %.2f of sugar\n"
					   + "You need %.2f of butter\n"
					   + "You need %.2f of flour", totalSugar, totalButter, totalFlour);
	}
}