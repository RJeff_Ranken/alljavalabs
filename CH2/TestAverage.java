import java.util.Scanner;

public class TestAverage
{
	public static void main (String[] args)
	{
		double test1;
		double test2;
		double test3;
		
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter the 1st score: ");
		test1 = keyboard.nextDouble();
		keyboard.nextLine();
		
		System.out.print("Enter the 2nd score: ");
		test2 = keyboard.nextDouble();
		keyboard.nextLine();
		
		System.out.print("Enter the 3rd score: ");
		test3 = keyboard.nextDouble();
		keyboard.nextLine();
		
		double average = (test1 + test2 + test3) / 3;
		
		System.out.printf("Test 1: %.2f\n"
						+ "Test 2: %.2f\n"
						+ "Test 3: %.2f\n"
						+ "AVG: %.2f", test1, test2, test3, average);
	}
}