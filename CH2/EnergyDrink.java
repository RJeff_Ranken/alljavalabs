import java.util.Scanner;

public class EnergyDrink
{
	public static void main(String[] args)
	{
		int customers = 15000;
	    double oneOrMore = 0.18;
		double citrusFlavor = 0.58;
		
		double energyDrinkConsumers = customers * oneOrMore;
		
		double citrusFlavorLovers = energyDrinkConsumers * citrusFlavor;
		
		System.out.printf("Customers: " + customers
						+"\nThose that buy one or more: " + (int)energyDrinkConsumers
						+"\nThose that bought one or more that likes citrus flavors: " + (int)citrusFlavorLovers);
	}
}