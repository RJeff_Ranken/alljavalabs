import java.util.Scanner;

public class MaleAndFemale
{
	public static void main (String[] args)
	{
		double male;
		double female;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter the amount of male students in class: ");
		male = keyboard.nextInt();
		keyboard.nextLine();
		
		System.out.print("Enter the amount of female students in class: ");
		female = keyboard.nextInt();
		keyboard.nextLine();
		
		double students = male + female;
		double malePercent = male / students;
		double femalePercent = female / students;
		
		System.out.printf("The amount of students you have: " + students + "\n" 
						+ male + " being male and " + female + " being female.\n"
						+ "the percent of males is: %.2f and the percent of females are %.2f", malePercent, femalePercent);
	}
}