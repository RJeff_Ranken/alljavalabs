public class SalesPrediction
{
	public static void main(String[] args)
	{
		double salesThisYear = 8300000;
		double percentOfTotalSales = 0.65;
		
		double salesEastCoastGenerated = salesThisYear * percentOfTotalSales;
		
		System.out.printf("The East Coast division will generate $%,.2f of $%,.2f", salesEastCoastGenerated, salesThisYear);		 
	}
}
