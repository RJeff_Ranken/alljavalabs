public class PersonalInformation
{
	public static void main(String[] args)
	{
		String name = "Richardo Jefferson";
		String address = "126 Kendall Bluff Ct.";
		String city = "Chesterfield";
		String state = "MO";
		int zip = 63017;
		String phoneNum = "314-494-8645";
		String collegeMajor = "Web Development";
		
		System.out.println(name 
						   + "\n" + address + " " + city + ", " + state + " " + zip
						   + "\n" + phoneNum
						   + "\n" + collegeMajor);
	}
}