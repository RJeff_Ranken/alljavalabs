public class NameAndInitials
{
	public static void main(String[] args)
	{
		String firstName = "Richardo";
		String middleName = "Lee";
		String lastName = "Jefferson";
		
		char firstInitial = 'R';
		char middleInitial = 'L';
		char lastInitial = 'J';
		
		System.out.println(firstInitial + " " + firstName);
		System.out.println(middleInitial + " " + middleName);
		System.out.println(lastInitial + " " + lastName);
	}
}