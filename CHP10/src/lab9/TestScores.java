package lab9;

import java.io.Serializable;

public class TestScores implements Serializable{

	private int[] numArray = new int[5];
	
	public int[] getNumArray() {
		return numArray;
	}

	public void setNumArray(int[] numArray) {
		this.numArray = numArray;
	}

	public TestScores(int[] scores)
	{
		for(int i = 0; i < scores.length; ++i)
		{
			numArray[i] = scores[i];
			if(scores[i] < 0 || scores[i] >100)
			{
				throw new IllegalArgumentException("Can't use a negative number or use a number greater than 100!"
													+"\nRerun the program and try again!");
			}
		}
	}
	
	public double getScoreAVG(int[] scores)
	{
		double avg;
		double scoreAcc = 0;
		
		for(int i = 0; i < scores.length; ++i)
		{
			numArray[i] = scores[i];
			scoreAcc += numArray[i];
		}
		
		avg = scoreAcc / scores.length;
		
		return avg;
	}
	
	
}
