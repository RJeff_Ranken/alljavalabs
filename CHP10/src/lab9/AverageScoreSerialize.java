package lab9;

import java.util.Scanner;

import java.io.*;

public class AverageScoreSerialize {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		int[] array = new int[5];
		
		Scanner keyboard = new Scanner(System.in);
		
		//System.out.print("How many numbers do you want to input? ");
		//numQTY = keyboard.nextInt();
		
		//array = new int[numQTY];

		
		for(int i = 0; i < array.length; i++)
		{
			System.out.print("Enter a integer: ");
			array[i] = keyboard.nextInt();
		}
			
		TestScores set1 = null;
		
		try {
			set1 = new TestScores(array);
		}
		catch(IllegalArgumentException ex)
		{
			System.out.println(ex.getMessage());
		}
				
		System.out.printf("The average of these scores is %.2f", set1.getScoreAVG(array));
		
		FileOutputStream outStream = new FileOutputStream("Objects.dat");
		ObjectOutputStream objectOutputFile = new ObjectOutputStream(outStream);
		
		for(int i = 0; i < array.length; i++)
		{
			objectOutputFile.writeObject(array[i]);
		}
		objectOutputFile.close();
		System.out.println("\nThe serialized array has been written to Objects.dat");
	}

}
