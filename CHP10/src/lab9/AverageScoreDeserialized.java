package lab9;

import java.io.*;

public class AverageScoreDeserialized {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		int[] ARRAY = new int [5];
		
		FileInputStream inStream = new FileInputStream("Objects.dat");
		ObjectInputStream objectInputFile = new ObjectInputStream(inStream);
		
		TestScores set1 = new TestScores(ARRAY);
		
		for (int i = 0; i < ARRAY.length; i++)
		{
			ARRAY[i] =  (int) objectInputFile.readObject();
		}
		
		objectInputFile.close();
		
		for (int i = 0; i < ARRAY.length; i++)
		{
			System.out.println(ARRAY[i]);
		}
	}
}
