package lab2;

public class InvalidTestScore extends Exception {

	public InvalidTestScore()
	{
		super("BLOOP! BLOOP! Can't use a negative number nor use a number higher than 100!"
				+ "\nShutting down the program! ");
	}
	
	public InvalidTestScore(int[] scores)
	{
		super("BLOOP! BLOOP! Can't use a negative number nor use a number higher than 100!"
				+ "\nShutting down the program! ");
	}
}
