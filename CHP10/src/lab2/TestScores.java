package lab2;

public class TestScores {

	private int[] numArray = new int[3];
	
	public int[] getNumArray() {
		return numArray;
	}

	public void setNumArray(int[] numArray) {
		this.numArray = numArray;
	}

	public TestScores(int[] scores) throws InvalidTestScore
	{
		for(int i = 0; i < scores.length; ++i)
		{
			if(numArray[i] < 1 || numArray[i] >100)
			{
				throw new InvalidTestScore(numArray);
			}
			numArray[i] = scores[i];
		}
	}
	
	public double getScoreAVG(int[] scores)
	{
		double avg;
		double scoreAcc = 0;
		
		for(int i = 0; i < scores.length; ++i)
		{
			numArray[i] = scores[i];
			scoreAcc += numArray[i];
		}
		
		avg = scoreAcc / scores.length;
		
		return avg;
	}
	
	
}

