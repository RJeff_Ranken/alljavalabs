package lab11;

public class Employee {

	private String name;
	private String employeeNumber;
	private String hireDate;
	
	public Employee(String n, String num, String date) throws InvalidEmployeeNumber
	{
		name = n;
		employeeNumber = num;
		hireDate = date;
	}
	
	public Employee()
	{
		
	}

	public void setName(String n) {
		name = n;
	}

	public void setEmployeeNumber(String e) {
		employeeNumber = e;
	}

	public void setHireDate(String h) {
		hireDate = h;
	}
	
	private boolean isValidEmpNum(String e)
	{
		boolean trueOrFalse;
		employeeNumber = e;
		int strConvert;
		
		strConvert = Integer.parseInt(employeeNumber);
		
		if(Character.isDigit(strConvert))
		{
			trueOrFalse = true;
		}
		else
		{
			trueOrFalse = false;
		}
		
		return trueOrFalse;
	}
	
	public String toString()
	{
		String str = "Employee Name: " + name +
					"\nEmployee Number: " + employeeNumber +
					"\nHire Date: " + hireDate;
		
		return str;
	}
}
