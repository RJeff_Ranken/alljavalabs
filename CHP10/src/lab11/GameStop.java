package lab11;

import java.util.Scanner;

public class GameStop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String nameInput;
		String numInput;
		String dateInput;
		int shiftInput;
		double rateInput;
		
		String empNameInput;
		int empNumberNumInput;
		String empNumberLetterInput;
		String hireInput;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter name of worker ");
		nameInput = keyboard.nextLine();
		System.out.print("Enter worker's ID ");
		numInput = keyboard.nextLine();
		System.out.print("Enter date ");
		dateInput = keyboard.nextLine();
		System.out.print("What shift did the worker take? type 1 for day or 2 for night?");
		shiftInput = keyboard.nextInt();
		System.out.print("Enter worker's pay rate ");
		rateInput = keyboard.nextDouble();
		ProductionWorker worker1;
		try 
		{
			worker1 = new ProductionWorker(nameInput, numInput, dateInput, shiftInput, rateInput);
		} 
		catch (InvalidShift | InvalidPayRate e) 
		{
			System.out.print("Your shift and/or pay rate entries are invalid");
		}
		
		//String workerInfo = "Name: " + nameInput.concat("\nID: " + numInput).concat("\n" + dateInput);
		//System.out.print(workerInfo + worker1.toString());
		
	}

}
