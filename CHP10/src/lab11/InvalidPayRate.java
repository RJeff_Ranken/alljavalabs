package lab11;

public class InvalidPayRate extends Exception{

	public InvalidPayRate()
	{
		super("Pay rate can't be negative");
	}
	public InvalidPayRate(double rateInput)
	{
		super("Pay rate can't be negative");
	}
}
