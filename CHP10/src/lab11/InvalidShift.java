package lab11;

public class InvalidShift extends Exception{

	public InvalidShift()
	{
		super("Day and night are two things. Use 1 for day or 2 for night. Thank you...");
	}
	public InvalidShift(int shiftNum)
	{
		super("Day and night are two things. Use 1 for day or 2 for night. Thank you...");
	}
}
