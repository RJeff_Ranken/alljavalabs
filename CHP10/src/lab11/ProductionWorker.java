package lab11;

public class ProductionWorker extends Employee{

	private int shift;
	private double payRate;
	public final int DAY_SHIFT = 1;
	public final int NIGHT_SHIFT = 2;
	
	public ProductionWorker(String n, String num, String date,
							int sh, double rate) throws InvalidShift, InvalidPayRate
	{
		shift = sh;
		payRate = rate;
		
		if(shift != DAY_SHIFT || shift != NIGHT_SHIFT)
		{
			throw new InvalidShift(shift);
		}
		if(payRate < 0)
		{
			throw new InvalidPayRate(payRate);
		}
	}
	
	public ProductionWorker()
	{
		
	}

	public void setShift(int s) {
		shift = s;
	}

	public void setPayRate(double p) {
		payRate = p;
	}
	
	public int getShift() {
		return shift;
	}

	public double getPayRate() {
		return payRate;
	}
	
	public String toString()
	{
		String str;
		
		str = String.format(shift + "\n" + payRate);
		
		return str;
	}
}
