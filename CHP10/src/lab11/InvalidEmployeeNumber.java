package lab11;

public class InvalidEmployeeNumber extends Exception{
	
	public InvalidEmployeeNumber()
	{
		super("Sorry! The number applied can't be less than 0 or higher than 9999");
	}

	public InvalidEmployeeNumber(int idNum)
	{
		super("Sorry! The number applied can't be less than 0 or higher than 9999");
	}
}
