package lab7;

import java.io.*;

public class FileRWMethods {

	boolean endOfFile = false;
	
	FileRWMethods()
	{
		
	}
	
	public static void writeString(String file) throws IOException
	{
		FileOutputStream fstream = new FileOutputStream(file);
		DataOutputStream outputFile =  new DataOutputStream(fstream);
		
		System.out.println("Writing to the file...");
		
		outputFile.close();
		System.out.println("File writing is done.");
	}
	
	public void readString(String file) throws IOException
	{
		String readString;
		
		FileInputStream fstream = new FileInputStream(file);
		DataInputStream inputFile = new DataInputStream(fstream);
		
		while(!endOfFile)
		{
			try
			{
				readString = inputFile.readUTF();
				System.out.print(readString);
			}
			catch(EOFException e)
			{
				endOfFile = true;
			}
			
			inputFile.close();
			System.out.print("Reading is done");
		}
	}
}
