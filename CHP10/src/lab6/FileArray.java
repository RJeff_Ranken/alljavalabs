package lab6;

import java.io.*;

public class FileArray {
	
	public FileArray()
	{
		
	}
	
	public static void writeArray(String fileName, int[] array) throws IOException
	{
		int[] numArray = new int[4];
		String textFile;
		textFile = fileName;
		for(int i = 0; i < array.length; ++i)
		{
			numArray[i] = array[i];
		}
		FileOutputStream fstream = new FileOutputStream(fileName);
		DataOutputStream outputFile = new DataOutputStream(fstream);
		
		System.out.println("Writing to the file...");
		
		for(int i = 0; i < array.length; i++)
		{
			outputFile.writeInt(array[i]);
		}
		
		outputFile.close();
		System.out.println("File writing is done.");
	}
	
	public void readArray(String fileName, int[] array) throws IOException
	{
		int num;
		boolean endOfFile = false;
		int[] numArray = new int[4];
		String textFile;
		textFile = fileName;
		
		FileInputStream fstream = new FileInputStream(fileName);
		DataInputStream inputFile = new DataInputStream(fstream);
		
		for(int i = 0; i < numArray.length; i++)
		{
			numArray[i] = array[i];
			while(!endOfFile)
			{
				
				try
				{
					array[i] = inputFile.readInt();
					System.out.print(array[i] + "\n");
				}
				catch(EOFException e)
				{
					endOfFile = true;
				}
			}
		}
		
		/*for(int z = 0; z < array.length; z++)
		{
			System.out.println(array[0]);
		}*/
		inputFile.close();
		System.out.println("Done reading.");
	}
}
