package lab6;

import java.io.IOException;
import java.util.Scanner;

public class FileWriteNRead {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		String file;
		int[] numInput = new int[4];

		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter a name for a text file and end it with .txt ");
		file = keyboard.nextLine();
		
		for(int i = 0; i < numInput.length; i++)
		{
			System.out.print("Enter a number: ");
			numInput[i] = keyboard.nextInt();
		}
		
		FileArray file1 = new FileArray();
		
		FileArray.writeArray(file, numInput);
		
		System.out.println("Reading numbers from the file: ");
		
		file1.readArray(file, numInput);
	}

}
