package lab3;

import java.util.Scanner;

public class RetailItemDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String descriptInput;
		int unitInput;
		double priceInput;
		
		String descriptInput2;
		int unitInput2;
		double priceInput2;
		
		String descriptInput3;
		int unitInput3;
		double priceInput3;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter description of item: ");
		descriptInput = keyboard.nextLine();
		
		System.out.print("Enter number of its units: ");
		unitInput = keyboard.nextInt();
		
		System.out.print("Enter price of item: ");
		priceInput = keyboard.nextDouble();
		
		RetailItem item1 = null;
		try
		{
			item1 = new RetailItem(descriptInput, unitInput, priceInput);
		}
		catch(NegativeQTY e)
		{
			System.out.print(e.getMessage());
		}
		catch(NegativePrice e)
		{
			System.out.print(e.getMessage());
		}
		
		/*System.out.print("Enter description of item: ");
		descriptInput2 = keyboard.nextLine();
		
		System.out.print("Enter number of its units: ");
		unitInput2 = keyboard.nextInt();
		
		System.out.print("Enter price of item: ");
		priceInput2 = keyboard.nextDouble();
		
		RetailItem item2 = null;
		try
		{
			item2 = new RetailItem(descriptInput2, unitInput2, priceInput2);
		}
		catch(NegativeQTY e)
		{
			System.out.print(e.getMessage());
		}
		catch(NegativePrice e)
		{
			System.out.print(e.getMessage());
		}*/
		System.out.printf("Item: " + descriptInput
						+"\nUnits: " + unitInput
						+"\nPrice: %.2f", priceInput);
	}

}
