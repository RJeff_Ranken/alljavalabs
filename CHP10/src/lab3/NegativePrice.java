package lab3;

public class NegativePrice extends Exception{

	public NegativePrice()
	{
		super("You input a negative price amount");
	}
	
	public NegativePrice(double p)
	{
		super("You input a negative price amount");
	}
}
