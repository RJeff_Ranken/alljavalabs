package lab3;

public class NegativeQTY extends Exception {

	public NegativeQTY()
	{
		super("You input a negative quantity");
	}
	
	public NegativeQTY(int q)
	{
		super("You input a negative quantity");
	}
}
