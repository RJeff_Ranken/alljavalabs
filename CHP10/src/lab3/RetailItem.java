package lab3;

public class RetailItem {

	private String description;
	private int unitsOnHand;
	private double price;
	
	public RetailItem(String des, int unit, double p) 
			throws NegativePrice,
				   NegativeQTY										
	{
		if(unit < 0)
		{
			throw new NegativeQTY(unit);
		}
		if(p < 0)
		{
			throw new NegativePrice(p);
		}
		
		description = des;
		unitsOnHand = unit;
		price = p;
	}
	public void setDesc(String d)
	{
		description = d;
	}
	
	public void setUnit(int u)
	{
		unitsOnHand = u;
	}
	
	public void setPrice(double p)
	{
		price = p;
	}
	
	public String getDesc()
	{
		return description;
	}
	
	public int getUnit()
	{
		return unitsOnHand;
	}
	
	public double getPrice()
	{
		return price;
	}
}

