package lab3;

public class Accounts {

	private int[] accNumbers = {5658845, 4520125, 7895122, 8777541, 8451277, 1302850,
								8080152, 4562555, 5552012, 5050552, 7825877, 1250255,
								1005231, 6545231, 3852085, 7576651, 7881200, 4581002};

	private static int index = 0;
	public boolean SearchAndFind(int accInput)
	{
		int element;
		boolean found;
		
		
		
		element = -1;
		found = false;
		
		while (!found && index < accNumbers.length)
		{
			if(accNumbers[index] == accInput)
			{
				found = true;
				element = index;
			}
			
			index++;
		}
		return found;
	}
}
