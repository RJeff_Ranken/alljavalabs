package lab1;

public class RainFall {

	private double[] monthlyRainfall = new double[12];
	
	public RainFall(double[] rain)
	{
		//monthlyRainfall = new double[rain.length];
		for(int i = 0; i < rain.length; ++i)
		{
			monthlyRainfall[i] = rain[i];
		}
	}

	public double[] getMonthlyRainfall() {
		return monthlyRainfall;
	}

	public void setMonthlyRainfall(double[] monthlyRainfall) {
		this.monthlyRainfall = monthlyRainfall;
	}
	
	public double getTotalRain(double[] rain)
	{
		double rainAcc = 0;
		for(int i = 0; i < rain.length; ++i)
		{
			monthlyRainfall[i] = rain[i];
			rainAcc += monthlyRainfall[i];
		}
		
		return rainAcc;
	}
	
	public double getAVGRain(double[] rain)
	{
		double AVG;
		double rainAcc = 0;
		for(int i = 0; i < rain.length; ++i)
		{
			monthlyRainfall[i] = rain[i];
			rainAcc += monthlyRainfall[i];
		}
		
		AVG = rainAcc / rain.length;
		
		return AVG;
	}
	
	public double getHighest(double[] rain)
	{
		double highest = monthlyRainfall[0];
		for(int i = 0; i < rain.length; ++i)
		{
			monthlyRainfall[i] = rain[i];
			if(monthlyRainfall[i] > highest)
				highest = monthlyRainfall[i];
		}	
		return highest;
	}
	
	public double getlowest(double[] rain)
	{
		double lowest = monthlyRainfall[0];
		for(int i = 0; i < rain.length; ++i)
		{
			monthlyRainfall[i] = rain[i];
			if(monthlyRainfall[i] < lowest)
				lowest = monthlyRainfall[i];
		}	
		return lowest;
	}
}
