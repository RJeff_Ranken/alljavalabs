package lab1;

import java.util.Scanner;

public class RainGainCanBeAPain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//double numInput;
		int year = 12;
		double rainTotal = 0;
		double[] numInput = new double[year];
		Scanner keyboard = new Scanner(System.in);
		
		for(int i = 0; i < year; ++i)
		{
			System.out.print("Enter rain amount for this month: ");
			numInput[i] = keyboard.nextDouble();
			while(numInput[i] < 0)
			{
				System.out.print("Rain amount cannot be negative"
								+"\nTry again: ");
				numInput[i] = keyboard.nextDouble();
			}
			rainTotal += numInput[i];
		}
		
		RainFall year1 = new RainFall(numInput);
		
		System.out.printf("The total rainfall is: %.2f", year1.getTotalRain(numInput));
		System.out.printf("\nThe average monthly rainfall is: %.2f", year1.getAVGRain(numInput));
		System.out.printf("\nThe highest rainfall is %.2f", year1.getHighest(numInput));
		System.out.printf("\nThe lowest rainfall is %.2f", year1.getlowest(numInput));
	}
}
