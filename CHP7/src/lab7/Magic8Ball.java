package lab7;

import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Magic8Ball {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		String question;
		String yesOrNo;
		int rand;
		int pullResponse;
		String fileOpen = "8_ball_responses.txt";
		
		ArrayList<String> responses = new ArrayList<String>();
		Scanner keyboard = new Scanner(System.in);
		
		File file = new File(fileOpen);
		Scanner inputFile = new Scanner(file);
		while (inputFile.hasNext())
		{
		    fileOpen = inputFile.nextLine();
			
			responses.add(fileOpen);
		}
		
		/*for(int index = 0; index < responses.size(); index++)
		{
			System.out.println(responses.get(index));
		}*/
		
		
		System.out.print("\nWhat do you want to ask the magic 8 ball?\n");
		question = keyboard.nextLine();
		
		Random ranResponse = new Random();
		
		rand = ranResponse.nextInt(11);
		
		System.out.println(responses.get(rand));
		
		System.out.print("\nWanna give it another go?" 
						+"\ntype yes to go again"
						+"\nor type no to quit ");
		yesOrNo = keyboard.nextLine();
		
		while(yesOrNo.equalsIgnoreCase("yes"))
		{
			System.out.print("\nWhat do you want to ask the magic 8 ball?\n");
			question = keyboard.nextLine();
			
			ranResponse = new Random();
			
			rand = ranResponse.nextInt(11);
			
			System.out.println(responses.get(rand));
			
			System.out.print("\nWanna give it another go?" 
					+"\ntype yes to go again"
					+"\nor type no to quit ");
			yesOrNo = keyboard.nextLine();
		}
		
		System.out.println("Ok. Take care!");
	}

}
