package lab4;

import java.util.ArrayList;

public class Larger {

	private int[] arrayNum1;
	
	public int[] getArrayNum1() {
		return arrayNum1;
	}

	public void setArrayNum1(int[] arrayNum1) {
		this.arrayNum1 = arrayNum1;
	}

	private int n;
	
	
	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public Larger(ArrayList<String> numArray)
	{
		for(int index = 0; index < numArray.size(); index++)
		{
			String thisIndex = numArray.get(index);
			int vertArray = Integer.parseInt(thisIndex);
			arrayNum1[index] = vertArray;
		}
	}
	
	public void GreaterThanN(ArrayList<String> numArray, int n)
	{
		for(int index = 0; index < numArray.size(); index++)
		{
			String thisIndex = numArray.get(index);
			int vertArray = Integer.parseInt(thisIndex);
			arrayNum1[index] = vertArray;
			
			if(arrayNum1[index] > n)
			{
				System.out.println(numArray.get(index));
			}
		}
	}
}
