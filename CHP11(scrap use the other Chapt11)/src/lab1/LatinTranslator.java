package lab1;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class LatinTranslator extends Application{

	private Label lbl1;
	private Label lbl2;
	private Label lbl3;
	
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		launch(args);
	}
	

	@Override
	public void start(Stage mainStage) throws Exception {
		// TODO Auto-generated method stub
		
		Button btn1 = new Button("Sinister");
		btn1.setOnAction(new ButtonClickHandler());
		
		Button btn2 = new Button("Dexter");
		
		
		Button btn3 = new Button("Medium");
		
		
		lbl1 = new Label("Left");
		lbl2 = new Label("Right");
		lbl3 = new Label("Center");
		
		
		GridPane grid1 = new GridPane();
		
		grid1.add(btn1, 0, 0);
		grid1.add(lbl1, 1, 0);
		grid1.add(btn2, 0, 1);
		grid1.add(lbl2, 1, 1);
		grid1.add(btn3, 0, 2);
		grid1.add(lbl3, 1, 2);
		
		Scene scene1 = new Scene(grid1, 300, 100); 
		
		mainStage.setScene(scene1);
		
		mainStage.setTitle("Latin Translator");
		
		mainStage.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent>
	@Override
	{
		public void handle(ActionEvent event)
		{
			lbl1.setText("This means left!");
		}
	}

}
