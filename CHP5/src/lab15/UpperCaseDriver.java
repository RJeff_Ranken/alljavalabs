package lab15;

import java.util.Scanner;
import java.io.*;

public class UpperCaseDriver {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		String filenameOpen;
		String filenameCreate;
		String gameTitle;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter a filename you want opened: ");
		filenameOpen = keyboard.nextLine();
		
		System.out.print("Enter a filename you want to save the new data to: ");
		filenameCreate = keyboard.nextLine();
		
		UpperCaseFile fileSet1 = new UpperCaseFile(filenameOpen, filenameCreate);
		
		fileSet1.setGames(filenameOpen);
		fileSet1.setGamesUpper(filenameCreate);
		
		System.out.print(fileSet1.getGames() + "\n");
		
		PrintWriter outputFile = new PrintWriter(filenameCreate);
		File file = new File(filenameOpen);
		if (!file.exists())
		{
			System.out.println(filenameOpen + " does not exist");
			
			System.exit(0);
		}
		
		Scanner inputFile = new Scanner(file);
		
		while (inputFile.hasNext())
		{
		    gameTitle = inputFile.nextLine();
			
			System.out.print(gameTitle.toUpperCase() + "\n");
		}
		

		
		
		inputFile.close();
		outputFile.close();
		
		System.out.print("\n\nNew file has been saved");
	}

}
