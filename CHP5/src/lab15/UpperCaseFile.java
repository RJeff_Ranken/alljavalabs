package lab15;

public class UpperCaseFile {

	private String games;
	private String gamesUpper;
	
	public UpperCaseFile(String gt, String gtUpper)
	{
		games = gt;
		gamesUpper = gtUpper;
	}

	public String getGames() {
		return games;
	}

	public String getGamesUpper() {
		return gamesUpper;
	}

	public void setGames(String gt) {
		games = gt;
	}

	public void setGamesUpper(String gtUpper) {
		gamesUpper = gtUpper;
	}
	
}
