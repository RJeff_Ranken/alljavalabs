package lab5;

public class HotelOcc {

	private int floor;
	private int rooms;
	private int occupied;
	
	public int getFloor() {
		return floor;
	}
	public int getRooms() {
		return rooms;
	}
	public int getOccupied() {
		return occupied;
	}
	public void setFloor(int f) {
		floor = f;
	}
	public void setRooms(int r) {
		rooms = r;
	}
	public void setOccupied(int o) {
		occupied = o;
	}
	
	
}
