package lab5;

import java.util.Scanner;

public class HotelDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int floorInput;
		int roomInput;
		int occupancyInput;
		int roomAccumulator = 0;
		int occupiedAccumulator = 0;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("How many floors are there in the hotel? ");
		floorInput = keyboard.nextInt();
		while(floorInput < 1)
		{
			System.out.print("Try again. Can not accept any numbers below 1 ");
			floorInput = keyboard.nextInt();
		}
		
		for(int i = 0; i < floorInput; i++)
		{
			System.out.print("how many rooms are on the floor? ");
			roomInput = keyboard.nextInt();
			
			while(roomInput < 10)
			{
				System.out.print("Try again. Can't be less than 10 rooms ");
				roomInput = keyboard.nextInt();
			}
			roomAccumulator += roomInput;
			
			
			System.out.print("How many of those rooms are occupied? ");
			occupancyInput = keyboard.nextInt();
			occupiedAccumulator += occupancyInput;
		}
		
		double occupancyRate = occupiedAccumulator / (double)roomAccumulator;
		
		System.out.printf("Total ammount of rooms: %d"
						+ "\nNumber of rooms occupied: %d"
						+ "\nOccupancy rate for the hotel: %.2f", roomAccumulator, occupiedAccumulator, occupancyRate);
	}

}
