package Lab2;

import java.util.Scanner;

public class DistanceDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int hourInput;
		int travelInput;
		int blankHR = 0;
		int blankMiles = 0;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("How many hours did the travel for? ");
		hourInput = keyboard.nextInt();
		while(hourInput < 0)
		{
			System.out.print("Try again. Can not accept negative numbers ");
			hourInput = keyboard.nextInt();
		}
		
		System.out.print("How many miles did the person travel per hour? ");
		travelInput = keyboard.nextInt();
		while(travelInput < 1)
		{
			System.out.print("Try again. Can not accept any numbers below 1 ");
			travelInput = keyboard.nextInt();
		}
		
		DistanceTraveled user1 = new DistanceTraveled(hourInput, travelInput);
		
		user1.getDistance(hourInput, travelInput);
		
		System.out.printf("%d miles were traveled", user1.getDistance(hourInput, travelInput));
		System.out.printf("\nHour" + "                     " + "Distance Traveled");
		
		int mph = user1.getDistance(hourInput, travelInput) / hourInput;
		while(blankHR < hourInput)
		{
			System.out.printf("\n%d %33d", blankHR += 1, blankMiles += mph);
		}
		
	
		
		/*while(blankMiles < user1.getDistance(hourInput, travelInput))
		{
			System.out.printf("\n%d", blankMiles += mph);
		}
		/*do
		{
			System.out.printf("\n%d %33d", hourInput, travelInput);
			hourInput++;
			travelInput++;
		}
		while(blankHR < hourInput);
		*/
	}

}
