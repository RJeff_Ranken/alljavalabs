package Lab2;

public class DistanceTraveled {

	private int mph;
	private int distanceTraveled;
	
	public DistanceTraveled(int miles, int dt)
	{
		mph = miles;
		distanceTraveled = dt;
	}
	
	public int getMph() {
		return mph;
	}
	public int getDistanceTraveled() {
		return distanceTraveled;
	}
	public void setMph(int miles) {
		mph = miles;
	}
	public void setDistanceTraveled(int dt) {
		distanceTraveled = dt;
	}
	
	public int getDistance(int miles, int dt)
	{
		mph = miles;
		distanceTraveled = dt;
		
		int distance = mph * distanceTraveled;
		
		return distance;
	}
}
