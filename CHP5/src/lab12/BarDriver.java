package lab12;

import java.util.Scanner;

public class BarDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int saleInput;
		int store = 1;
		
		Scanner keyboard = new Scanner(System.in);

		while(store < 6)
		{
			System.out.printf("Enter today's sales for store %d: ", store);
			saleInput = keyboard.nextInt();
			
			 int starQTY = saleInput / 100;
			System.out.printf("Store %d: ", store);
			for(int i = 0; i < starQTY; i++)
			{
				System.out.print("*");
			}
			System.out.print("\n");
			store++;
		}
		
	}

}
