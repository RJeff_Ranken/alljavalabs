package lab1;

import java.util.Scanner;

public class SumNum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int numInput;
	    int X = 0;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter a number to get a count up to that point: ");
		numInput = keyboard.nextInt();
		
		do
		{
			System.out.printf("\n%d", X += 1);
		}
		while (X < numInput);
	}

}
