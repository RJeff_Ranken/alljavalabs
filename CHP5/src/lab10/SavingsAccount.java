package lab10;

public class SavingsAccount {

	private double startBalance;
	private double annualRate;
	private double balance;
	private double withdraw;
	private double deposit;
	
	public SavingsAccount(double sb)
	{
		startBalance = sb;
	}
	
	public SavingsAccount()
	{
		
	}

	public void setAnnualRate(double ar) {
		annualRate = ar;
	}

	public void setBalance(double b) {
		balance = b;
	}

	public void setStartBalance(double sb) {
		startBalance = sb;
	}
	
	public void setWithdraw(double w) {
		withdraw = w;
	}

	public void setDeposit(double d) {
		deposit = d;
	}
	
	public double getAnnualRate() {
		return annualRate;
	}

	public double getBalance() {
		return balance;
	}

	public double getStartBalance() {
		return startBalance;
	}

	public double getWithdraw() {
		return withdraw;
	}

	public double getDeposit() {
		return deposit;
	}

	public double monthWithdrawl(double b, double w)
	{
		balance = b;
		withdraw = w;
		
		double subtract = balance - withdraw;
		return subtract;
	}
	
	public double monthDeposit(double b, double d)
	{
		balance = b;
		deposit = d;
		
		double add = balance + deposit;
		return add;
	}
	
	public double getMonthlyInterest(double b, double ar)
	{
		balance = b;
		annualRate = ar;
		
		double monthlyRate = annualRate / 12;
		
		double monthlyInterest = (balance * monthlyRate) + balance;
		
		return monthlyInterest;
	}
}
