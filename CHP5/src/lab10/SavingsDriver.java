package lab10;

import java.util.Scanner;

public class SavingsDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double savingsInput;
		double annualInterestRate;
		int monthInput;
		double withdrawAcc = 0;
		double depositAcc = 0;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("What is your annual interest rate? ");
		annualInterestRate = keyboard.nextDouble();
		
		SavingsAccount user1 = new SavingsAccount();
		
		user1.setAnnualRate(annualInterestRate);
		
		double monthlyIntRate = user1.getAnnualRate() / 12;
		
		System.out.print("What is your current balance? ");
		savingsInput = keyboard.nextDouble();
		
		user1.setStartBalance(savingsInput);
		double balance = user1.getStartBalance();
		user1.setBalance(balance);
		
		System.out.print("How many months have passed since the account was made? ");
		monthInput = keyboard.nextInt();
		
		for(int x = 0; x < monthInput; x++)
		{
			double depositBase = depositAcc - 0;
			double withdrawBase = withdrawAcc - 0;
			double newBal;
			
			System.out.print("How much did you deposit this month? ");
			depositAcc = keyboard.nextDouble();			
			user1.monthDeposit(balance, depositAcc);
			user1.setBalance(user1.monthDeposit(balance, depositAcc));
			System.out.printf("$%.2f\n", user1.getBalance());
			newBal = user1.getBalance();
			user1.setBalance(newBal);
			//depositAcc += balance;
			depositAcc += depositBase;
			
			System.out.print("How much did you withdraw this month? ");
			withdrawAcc = keyboard.nextDouble();
			user1.monthWithdrawl(balance, withdrawBase);
			user1.setBalance(user1.monthWithdrawl(balance, withdrawAcc));
			System.out.printf("$%.2f\n", user1.getBalance());
			//withdrawAcc -= balance;
			withdrawAcc += withdrawBase;
			
			System.out.printf("Monthly Interest: $%.2f\n", user1.getMonthlyInterest(balance, annualInterestRate));
		}
		
		System.out.printf("Ending Balance: $%.2f"
				         +"\nTotal amount of deposits: $%.2f"
				         +"\nTotal amount of withdraws: $%.2f"
				         /*+"\nTotal interest earned: $%.2f"*/, balance, depositAcc, withdrawAcc);
	}

}
