package Lab3;

public class DistanceFile {

	private int mph;
	private int distanceTraveled;
	
	public DistanceFile(int miles, int dt)
	{
		mph = miles;
		distanceTraveled = dt;
	}
	
	public int getMph() {
		return mph;
	}
	public int getDistanceTraveled() {
		return distanceTraveled;
	}
	public void setMph(int miles) {
		mph = miles;
	}
	public void setDistanceTraveled(int dt) {
		distanceTraveled = dt;
	}
	
	public int getDistance(int miles, int dt)
	{
		mph = miles;
		distanceTraveled = dt;
		
		int distance = mph * distanceTraveled;
		
		return distance;
	}
}
