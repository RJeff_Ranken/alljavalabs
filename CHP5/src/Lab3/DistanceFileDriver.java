package Lab3;

import java.util.Scanner;
import java.io.*;

public class DistanceFileDriver {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		int hourInput;
		int travelInput;
		int blankHR = 0;
		int blankMiles = 0;
		String filename;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("How many hours did the travel for? ");
		hourInput = keyboard.nextInt();
		while(hourInput < 0)
		{
			System.out.print("Try again. Can not accept negative numbers ");
			hourInput = keyboard.nextInt();
		}
		
		System.out.print("How many miles did the person travel per hour? ");
		travelInput = keyboard.nextInt();
		while(travelInput < 1)
		{
			System.out.print("Try again. Can not accept any numbers below 1 ");
			travelInput = keyboard.nextInt();
		}
		
		DistanceFile user1 = new DistanceFile(hourInput, travelInput);
		
		user1.getDistance(hourInput, travelInput);
		
		PrintWriter outputFile = new PrintWriter("DistanceFile.txt");
		
		System.out.printf("%d miles were traveled", user1.getDistance(hourInput, travelInput));
		System.out.printf("\nHour" + "                     " + "Distance Traveled");
		
		int mph = user1.getDistance(hourInput, travelInput) / hourInput;
		    outputFile.println("Hour"  + "           " + "Distance Traveled");
		while(blankHR < hourInput)
		{
			System.out.printf("\n%d %33d", blankHR += 1, blankMiles += mph);
			outputFile.println(blankHR + "                             " + blankMiles);
		}
		
		outputFile.close();
		System.out.print("\n\nFile saved to DistanceFile.txt");
	}

}
