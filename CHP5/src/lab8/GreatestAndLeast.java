package lab8;

public class GreatestAndLeast {

	private int numInput;
	private int greatestNum;
	private int leastNum;
	
	public int getNumInput() {
		return numInput;
	}
	public int getGreatestNum() {
		return greatestNum;
	}
	public int getLeastNum() {
		return leastNum;
	}
	public void setNumInput(int num) {
		numInput = num;
	}
	public void setGreatestNum(int g) {
		greatestNum = g;
	}
	public void setLeastNum(int l) {
		leastNum = l;
	}
	
	
}
