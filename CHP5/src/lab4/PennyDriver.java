package lab4;

import java.util.Scanner;
import java.text.DecimalFormat;

public class PennyDriver {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int dayInput;
		int blankDay = 0;
		double salary = 0.01;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("How many days did you work for? ");
		dayInput = keyboard.nextInt();
		
		Pennies worker1 = new Pennies(dayInput);
		
		System.out.printf("\nDay" + "     " + "Salary");
		
		double doubleSal = salary;
		
		while(blankDay < dayInput)
		{
			System.out.printf("\n%d       $%.2f", blankDay += 1, doubleSal);
			doubleSal += .01 * .5;
		}
	}

}
