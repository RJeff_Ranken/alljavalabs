package lab4;

public class Pennies {

	private int numDays;
	
	public Pennies(int d)
	{
		numDays = d;
	}

	public int getNumDays() {
		return numDays;
	}

	public void setNumDays(int d) {
		numDays = d;
	}
	
	public double getSalary()
	{
		double total = .01 * 2;
		
		return total;
	}
}
