package LAB18;

import java.util.Scanner;

public class RouletteDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int numInput = 0;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter a number ranging from 0 to 36 or 00: ");
		numInput = keyboard.nextInt();
		
		RoulettePocket num1 = new RoulettePocket(numInput);
		
		if(numInput == 0)
		{
			System.out.printf("%s", num1.getPocketColor(numInput));
		}
		else if(numInput == 00)
		{
			System.out.printf("%s", num1.getPocketColor(numInput));
		}
		else if (numInput < 0 || numInput > 36)
		{
			System.out.print("ERROR");
		}
		else
		{
			System.out.printf("%d's color is %s", numInput, num1.getPocketColor(numInput));
		}
	}

}
