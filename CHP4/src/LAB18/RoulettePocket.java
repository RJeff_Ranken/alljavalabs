package LAB18;

public class RoulettePocket {

	private int pocketNumber;
	
	public RoulettePocket(int n)
	{
		pocketNumber = n;
	}

	public int getPocketNumber() {
		return pocketNumber;
	}

	public void setPocketNumber(int n) {
		pocketNumber = n;
	}
	
	public String getPocketColor(int n)
	{
		pocketNumber = n;
		String color = "";
		if(pocketNumber == 0)
		{
			color = "0 is Green";
		}
		if(pocketNumber == 00) {
			color = "00 is Green";
		}
		if(pocketNumber >=1 && pocketNumber <=10)
		{
			if(pocketNumber == 1 || pocketNumber == 3 || pocketNumber == 5 || pocketNumber == 7 || pocketNumber == 9)
			{
				color = "Red";
			}
			else
			{
				color = "Black";
			}
		}
		if(pocketNumber >=11 && pocketNumber <=18)
		{
			if(pocketNumber == 11 || pocketNumber == 13 || pocketNumber == 15 || pocketNumber == 17)
			{
				color = "Black";
			}
			else
			{
				color = "Red";
			}
		}
		if(pocketNumber >=19 && pocketNumber <=28)
		{
			if(pocketNumber == 19 || pocketNumber == 21 || pocketNumber == 23 || pocketNumber == 25 || pocketNumber == 27)
			{
				color = "Red";
			}
			else
			{
				color = "Black";
			}
		}
		if(pocketNumber >=29 && pocketNumber <=36)
		{
			if(pocketNumber == 29 || pocketNumber == 31 || pocketNumber == 33 || pocketNumber == 35)
			{
				color = "Black";
			}
			else
			{
				color = "Red";
			}
		}
		return color;
	}
	
}
