package LAB15;

import java.util.Scanner;

public class BookClubPoints {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int userInput;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("How many books have you purchased this month? ");
		userInput = keyboard.nextInt();
		
		if (userInput < 0)
		{
			System.out.println("ERROR!");
		}
		else if (userInput == 0)
		{
			System.out.println("No points this time. Sorry.");
		}
		else if (userInput == 1)
		{
			System.out.println("Congrats! You get 5 points!");
		}
		else if (userInput == 2)
		{
			System.out.println("Congrats! You get 15 points!");
		}
		else if (userInput == 3)
		{
			System.out.println("Congrats! You get 30 points!");
		}
		else
		{
			System.out.println("Congrats! You get 60 points!");
		}
	}

}
