package Lab5;

import java.util.Scanner;

public class BankChargesDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int checkQTY;
		double balance;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter your balance: ");
		balance = keyboard.nextDouble();
		
		System.out.print("Enter the number of checks you used: ");
		checkQTY = keyboard.nextInt();
		
		BankCharges balance1 = new BankCharges(balance, checkQTY);
		
		System.out.printf("$%.2f", balance1.bankFees(balance, checkQTY));
	}

}
