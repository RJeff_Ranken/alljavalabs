package Lab5;

public class BankCharges {
	
	private int numOfChecks;
	private double endBalance;
	
	public BankCharges(double b, int QTY)
	{
		endBalance = b;
		numOfChecks = QTY;
	}
	
	public void setNumOfChecks(int n) {
		numOfChecks = n;
	}
	public void setEndBalance(double e) {
		endBalance = e;
	}
	
	public int getNumOfChecks() {
		return numOfChecks;
	}
	public double getEndBalance() {
		return endBalance;
	}
	
	public double bankFees(double b, int QTY)
	{
		endBalance = b;
		numOfChecks = QTY;
		double checkFees = 0;
		if (endBalance < 400)
		{
			endBalance -= 15;
		}
		
		if (numOfChecks < 20)
		{
			
			checkFees = numOfChecks * .10;
		}
		if (numOfChecks >= 20 && numOfChecks <= 39)
		{
			
			checkFees = numOfChecks * .08;
		}
		if (numOfChecks >= 40 && numOfChecks <= 59)
		{
			
			checkFees = numOfChecks * .06;
		}
		if (numOfChecks >= 60)
		{
			
			checkFees = numOfChecks * .04;
		}
		
		double total = endBalance - checkFees;
		return total;
	}
}
