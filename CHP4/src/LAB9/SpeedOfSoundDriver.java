package LAB9;

import java.util.Scanner;

public class SpeedOfSoundDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String userInput;
		String a = "air";
		String w = "water";
		String s = "steel";
		int numInput;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("What medium do you want to use to get the speed of sound?"
						+ "\nair, water, or steel? ");
		userInput = keyboard.nextLine();
		
		if(userInput.equals(a))
		{
			System.out.print("Please input the distance (in ft): ");
			numInput = keyboard.nextInt();
			
			SpeedOfSound ex1 = new SpeedOfSound(numInput);
			
			System.out.printf("The time it will take by air: %.2fsecs", ex1.getSpeedInAir());
		}
		else if(userInput.equals(w))
		{
			System.out.print("Please input the distance (in ft): ");
			numInput = keyboard.nextInt();
			
			SpeedOfSound ex1 = new SpeedOfSound(numInput);
			
			System.out.printf("The time it will take by water: %.2fsecs", ex1.getSpeedInWater());
		}
	    else if(userInput.equals(s))
		{
			System.out.print("Please input the distance (in ft): ");
			numInput = keyboard.nextInt();
			
			SpeedOfSound ex1 = new SpeedOfSound(numInput);
			
			System.out.printf("The time it will take by steel: %.2fsecs", ex1.getSpeedInSteel());
		}
		else
		{
			System.out.println("Sorry. This is not one of the options!");
		}
	}

}
