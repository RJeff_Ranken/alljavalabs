package LAB9;

public class SpeedOfSound {

	public double distance;

	public SpeedOfSound(double d)
	{
		distance = d;
	}
	
	public void setDistance(double d) {
		distance = d;
	}
	
	public double getDistance() {
		return distance;
	}
	
	public double getSpeedInAir()
	{
		double Time;
		
		Time = distance/1100;
		
		return Time;
	}
	
	public double getSpeedInWater()
	{
		double Time;
		
		Time = distance/4900;
		
		return Time;
	}
	
	public double getSpeedInSteel()
	{
		double Time;
		
		Time = distance/16400;
		
		return Time;
	}
}
