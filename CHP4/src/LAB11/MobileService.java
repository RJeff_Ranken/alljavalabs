package LAB11;

public class MobileService {

	private char phonePackage;
	private int minutesUsed;
	
	public MobileService(char p, int QTY)
	{
		phonePackage = p;
		minutesUsed = QTY;
	}
	
	public void setPhonePackage(char phone) {
		phonePackage = phone;
	}
	public void setMinutesUsed(int minutes) {
		minutesUsed = minutes;
	}
	
	public char getPhonePackage() {
		return phonePackage;
	}
	public int getMinutesUsed() {
		return minutesUsed;
	}
	
	public double subPackage(char p, int num)
	{
		double packageBase = 0;
		double packageUsed = 0;
		phonePackage = p;
		minutesUsed = num;
		
		if (phonePackage == 'A' || phonePackage == 'a')
		{
			packageBase = 39.99;
			
			if(minutesUsed > 450)
			{
				packageUsed = (minutesUsed - 450) * .45;
			}
		}
		
		if (phonePackage == 'B' || phonePackage == 'b')
		{
			packageBase = 59.99;
			
			if(minutesUsed > 900)
			{
				packageUsed = (minutesUsed - 900) * .40;
			}
		}
		
		if (phonePackage == 'C' || phonePackage == 'c')
		{
			packageBase = 69.99;
		}
		
		double total = packageBase += packageUsed;
		return total;
	}
}
