package LAB11;

import java.util.Scanner;

public class MobileServiceDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int minUsed;
		char letterInput;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Which package did you purchase this month? (A, B, or C) ");
		letterInput = keyboard.next().charAt(0);
		
		System.out.print("How many minutes did you use in that package? ");
		minUsed = keyboard.nextInt();
		
		MobileService cust1 = new MobileService(letterInput, minUsed);
		
		System.out.printf("Total Charge: $%.2f", cust1.subPackage(letterInput, minUsed));
	}

}
