package lab20;

import java.util.Scanner;

public class RestaurantDriver {

	public static void main(String[] args)
	{
		Scanner keyboard = new Scanner(System.in);
		
		Restaurant joes = new Restaurant(false,false,false);
		joes.setName("Joe's Gourmet Burgers");
		
		Restaurant mainStreet = new Restaurant(true,false,true);
		mainStreet.setName("Main Street Pizza Company");
		
		Restaurant cornerCafe = new Restaurant(true,true,true);
		cornerCafe.setName("Corner Cafe");
		
		Restaurant mamas = new Restaurant(true,false,false);
		mamas.setName("Mama's Fine Italian");
		
		Restaurant chefsKitchen = new Restaurant(true,true,true);
		chefsKitchen.setName("The Chefs Kitchen");
		
	
		boolean isVegetarian=false;
		boolean isVegan=false;
		boolean isGlutenFree=false;
			
		
		System.out.println("Is anyone a vegetarian?");
		String vegetarian = keyboard.nextLine();
		
		
		System.out.println("Is anyone a vegan?");
		String vegan = keyboard.nextLine();
		
		
		System.out.println("Is anyone a Gluten Free?");
		String glutenFree = keyboard.nextLine();
		
		if(vegetarian.equalsIgnoreCase("yes"))
		{
			isVegetarian = true;
		}
		
		if(vegan.equalsIgnoreCase("yes"))
		{
			isVegan = true;
		}
		
		if(glutenFree.equalsIgnoreCase("yes"))
		{
			isGlutenFree = true;
		}
		
		boolean isJoesGood = joes.determineOptions(isVegetarian, isVegan, isGlutenFree);
		boolean isMainStGood = mainStreet.determineOptions(isVegetarian, isVegan, isGlutenFree);
		boolean isCornerCafe = cornerCafe.determineOptions(isVegetarian, isVegan, isGlutenFree);
		boolean isMamasGood = mamas.determineOptions(isVegetarian, isVegan, isGlutenFree);
		boolean isChefsGood = chefsKitchen.determineOptions(isVegetarian, isVegan, isGlutenFree);

		String validRestaurants="";
		
		if(isJoesGood)
		{
			validRestaurants += joes.getName() + "\n";
		}
		
		if(isMainStGood)
		{
			validRestaurants += mainStreet.getName() + "\n";
		}
		
		if(isCornerCafe)
		{
			validRestaurants += cornerCafe.getName() + "\n";
		}
		
		if(isMamasGood)
		{
			validRestaurants += mamas.getName() + "\n";
		}
		
		if(isChefsGood)
		{
			validRestaurants += chefsKitchen.getName() + "\n";
		}
		
		System.out.println(validRestaurants);
		
		
		
		/*if(isVegetarian.equalsIgnoreCase("yes"))
		{
			if(isVegan.equalsIgnoreCase("yes"))
			{
				if(isGlutenFree.equalsIgnoreCase("yes"))
				{
					if(joes.getIsGlutenFree() && joes.getIsVegan() && joes.getIsGlutenFree())
					{
						validRestaurants += "Joes Gourmet Burgers";
					}
					if(mainStreet.getIsGlutenFree() && mainStreet.getIsVegan() && mainStreet.getIsGlutenFree())
					{
						validRestaurants += "Main Street Pizza Company";
					}
					if(cornerCafe.getIsGlutenFree() && cornerCafe.getIsVegan() && cornerCafe.getIsGlutenFree())
					{
						validRestaurants += "Corner Cafe";
					}
					if(chefsKitchen.getIsGlutenFree() && chefsKitchen.getIsVegan() && chefsKitchen.getIsGlutenFree())
					{
						validRestaurants += "Chefs Kitchen";
					}
				}
				else
				{
					
				}
			}
			else
			{
				
			}
			
		}
		else
		{
			
		}*/
		
	
		
		
		
	}
}
