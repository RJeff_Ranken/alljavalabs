package lab20;

public class Restaurant {

	private String name;
	private boolean isVegetarian;
	private boolean isVegan;
	private boolean isGlutenFree;
	
	public Restaurant(boolean vegetarian,boolean vegan,boolean glutenFree)
	{
		isVegetarian = vegetarian;
		isVegan = vegan;
		isGlutenFree = glutenFree;
	}
	
	public boolean getIsVegetarian()
	{
		return isVegetarian;
	}
	
	public boolean getIsVegan()
	{
		return isVegan;
	}
	
	public boolean getIsGlutenFree()
	{
		return isGlutenFree;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public boolean determineOptions(boolean vegetarian,boolean vegan,boolean glutenFree)
	{
		if(vegetarian)
		{
			if(!(this.isVegetarian))
			{
				return false;
			}
		}
		
		if(vegan)
		{
			if(!(this.isVegan))
			{
				return false;
			}
		}
		
		if(glutenFree)
		{
			if(!(this.isGlutenFree))
			{
				return false;
			}
		}
		
		return true;
		
		
	/*	
		
		if(this.isVegetarian == vegetarian && this.isVegan == vegan && this.isGlutenFree == glutenFree)
		{
			value= true;
		}
		else
		{
			value= false;
		}
		return value;*/
	}
	
}
