package LAB10;

public class FreezingBoilingPoints {

	public int temperature;

	public FreezingBoilingPoints(int t)
	{
		temperature = t;
	}
	
	public void setTemperature(int t) {
		temperature = t;
	}
	
	public int getTemperature() {
		return temperature;
	}
	
	public boolean isEthylFreezing()
	{
		boolean eFreeze;
		if(temperature <= -173)
		{
			eFreeze = true;
		}
		else
		{
			eFreeze = false;
		}
		
		return eFreeze;
	}
	
	public boolean isEthylBoiling()
	{
		boolean eMelt;
		if(temperature >= 172)
		{
			eMelt = true;
		}
		else
		{
			eMelt = false;
		}
		
		return eMelt;
	}
	
	public boolean isOxygenFreezing()
	{
		boolean oFreeze;
		if(temperature <= -362)
		{
			oFreeze = true;
		}
		else
		{
			oFreeze = false;
		}
		
		return oFreeze;
	}
	
	public boolean isOxygenBoiling()
	{
		boolean oMelt;
		if(temperature >= -306)
		{
			oMelt = true;
		}
		else
		{
			oMelt = false;
		}
		
		return oMelt;
	}
		
	public boolean isWaterFreezing()
	{
		boolean wFreeze;
		if(temperature <= 32)
		{
			wFreeze = true;
		}
		else
		{
			wFreeze = false;
		}
			
		return wFreeze;
	}
		
	public boolean isWaterBoiling()
	{
		boolean wMelt;
		if(temperature >= 212)
		{
			wMelt = true;
		}
		else
		{
			wMelt = false;
		}
			
		return wMelt;
	}
}
