package LAB3;

public class TestScores {

	public double test1;
	public double test2;
	public double test3;
	
	public TestScores(double t1, double t2, double t3)
	{
		test1 = t1;
		test2 = t2;
		test3 = t3;
	}
	
	public void setTest1(double t1) {
		test1 = t1;
	}
	public void setTest2(double t2) {
		test2 = t2;
	}
	public void setTest3(double t3) {
		test3 = t3;
	}
	
	public double getTest1() {
		return test1;
	}
	public double getTest2() {
		return test2;
	}
	public double getTest3() {
		return test3;
	}
	
	public double average(double one, double two, double three)
	{
		test1 = one;
		test2 = two;
		test3 = three;
		double average = (one + two + three) / 3;
		
		return average;
	}
	
	/*public String letterGrade()
	{
		char a = 'A';
		char b = 'B';
		char c = 'C';
		char d = 'D';
		char f = 'F';
		
		int A = 90;
		int B = 80;
		int C = 70;
		int D = 60;
		
		if 
	}*/
}
