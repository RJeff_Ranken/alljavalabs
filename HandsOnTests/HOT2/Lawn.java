package HOT2;

public class Lawn {

	private int width;
	private int length;
	
	public Lawn()
	{
		
	}
	
	public Lawn(int w, int l)
	{
		width = w;
		length = l;
	}

	public void setWidth(int w) {
		width = w;
	}

	public void setLength(int l) {
		length = l;
	}
	
	public int getWidth() {
		return width;
	}

	public int getLength() {
		return length;
	}
	
	public int getSqFt(int w, int l)
	{
		width = w;
		length = l;
		
		int sqFt = (width * 2) + (length * 2);
		
		return sqFt;
	}
}
