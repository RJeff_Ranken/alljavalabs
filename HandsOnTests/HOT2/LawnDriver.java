package HOT2;

import java.util.Scanner;

public class LawnDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//For lawn 1
		int wid1;
		int len1;

		//For lawn 2
		int wid2;
		int len2;
		
		Scanner keyboard = new Scanner(System.in);
		
		/*****************
		 All for 1st lawn
		*****************/
		System.out.print("Enter the width of the 1st lawn: ");
		wid1 = keyboard.nextInt();
		
		System.out.print("Enter the length of the 1st lawn: ");
		len1 = keyboard.nextInt();
		
		Lawn lawn1 = new Lawn(wid1, len1);
		
		if(lawn1.getSqFt(wid1, len1) < 400)
		{
			double under400 = 25.00;
			
			double totalFee = under400 * 20;
			
			System.out.printf("%d sqft", lawn1.getSqFt(wid1, len1));
			System.out.printf("\nWeekly fee is $25");
			System.out.printf("\ntotal for first lawn: $%.2f", totalFee);
		}
		if(lawn1.getSqFt(wid1, len1) >= 400 && lawn1.getSqFt(wid1, len1) < 600)
		{
			double between400And600 = 35.00;
			
			double totalFee = between400And600 * 20;
			
			System.out.printf("%d sqft", lawn1.getSqFt(wid1, len1));
			System.out.printf("\nWeekly fee is $35");
			System.out.printf("\ntotal for first lawn: $%.2f", totalFee);
		}
		if(lawn1.getSqFt(wid1, len1) >= 600)
		{
			double equalOrAbove600 = 50.00;
			
			double totalFee = equalOrAbove600 * 20;
			
			System.out.printf("%d sqft", lawn1.getSqFt(wid1, len1));
			System.out.printf("\nWeekly fee is $50");
			System.out.printf("\ntotal for first lawn: $%.2f", totalFee);
		}
		
		
		/*****************
		 All for 2nd lawn
		*****************/		
		Lawn lawn2 = new Lawn();
		
		System.out.print("\n\nEnter the width of the 2nd lawn: ");
		wid2 = keyboard.nextInt();
		
		System.out.print("Enter the length of the 2nd lawn: ");
		len2 = keyboard.nextInt();
		
		lawn2.setWidth(wid2);
		lawn2.setLength(len2);
		
		if(lawn2.getSqFt(lawn2.getWidth(), lawn2.getLength()) < 400)
		{
			double under400 = 25.00;
			
			double totalFee = under400 * 20;
			
			System.out.printf("%d sqft", lawn2.getSqFt(lawn2.getWidth(), lawn2.getLength()));
			System.out.printf("\nWeekly fee is $25");
			System.out.printf("\ntotal for first lawn: $%.2f", totalFee);
		}
		if(lawn2.getSqFt(lawn2.getWidth(), lawn2.getLength()) >= 400 && lawn2.getSqFt(lawn2.getWidth(), lawn2.getLength()) < 600)
		{
			double between400And600 = 35.00;
			
			double totalFee = between400And600 * 20;
			
			System.out.printf("%d sqft", lawn2.getSqFt(lawn2.getWidth(), lawn2.getLength()));
			System.out.printf("\nWeekly fee is $35");
			System.out.printf("\ntotal for first lawn: $%.2f", totalFee);
		}
		if(lawn2.getSqFt(lawn2.getWidth(), lawn2.getLength()) >= 600)
		{
			double equalOrAbove600 = 50.00;
			
			double totalFee = equalOrAbove600 * 20;
			
			System.out.printf("%d sqft", lawn2.getSqFt(lawn2.getWidth(), lawn2.getLength()));
			System.out.printf("\nWeekly fee is $50");
			System.out.printf("\ntotal for first lawn: $%.2f", totalFee);
		}
	}

}
