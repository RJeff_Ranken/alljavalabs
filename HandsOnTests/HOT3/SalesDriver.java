package HOT3;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class SalesDriver {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		double totalAcc = 0;
		int blank = 0;
		double salesInput;
		
		Scanner keyboard = new Scanner(System.in);
		PrintWriter outputFile = new PrintWriter("Sales.txt");
		
		System.out.print("In the next prompt, please enter 5 sales.\n\n");
		for(int i = 0; i < 5; i++)
		{
			
			System.out.printf("Enter sale: ");
			salesInput = keyboard.nextDouble();
			while(salesInput < 0)
			{
				System.out.print("Sorry! You can't input negative numbers."
						+ "\nTry again! ");
				salesInput = keyboard.nextDouble();
			}
			outputFile.println(salesInput);
			totalAcc += salesInput;
		}
		
		System.out.printf("\nTotal Sales: $%.2f", totalAcc);
		outputFile.printf("\nTotal Sales: $%.2f", totalAcc);
		
		outputFile.close();
		System.out.print("\n\nFile saved to Sales.txt");
	}

}
