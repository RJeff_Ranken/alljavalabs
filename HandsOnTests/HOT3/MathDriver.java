package HOT3;

import java.util.Scanner;

public class MathDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int input1, input2, input3;
		int insert;
		int select;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Press 1 to add to numbers or press 2 to add 3 numbers: ");
		select = keyboard.nextInt();
		
		if(select == 1)
		{
			System.out.print("Enter your first number: ");
			input1 = keyboard.nextInt();
			System.out.print("Now enter your second number: ");
			input2 = keyboard.nextInt();
			insert = Math.add2(input1, input2);
			System.out.printf("The numbers you put in totals to %d", insert);
		}
		if(select == 2)
		{
			System.out.print("Enter your first number: ");
			input1 = keyboard.nextInt();
			System.out.print("Now enter your second number: ");
			input2 = keyboard.nextInt();
			System.out.print("Finally enter your last number: ");
			input3 = keyboard.nextInt();
			insert = Math.add3(input1, input2, input3);
			System.out.printf("The numbers you put in totals to %d", insert);
		}
		if(select != 1 || select != 2)
		{
			System.out.print("Use only 1 or 2 please and thank you!"
							+"\nPROGRAM TERMINATED!");
		}
	}

}
