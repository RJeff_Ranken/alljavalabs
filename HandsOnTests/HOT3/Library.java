package HOT3;

public class Library {

	private Book bookInfo;
	private String libName;
	
	public Library(String ln, Book bk1)
	{
		bookInfo = bk1;
		libName = ln;
	}

	public Book getBookInfo() {
		return bookInfo;
	}

	public String getLibName() {
		return libName;
	}

	public void setBookInfo(Book bookInfo) {
		this.bookInfo = bookInfo;
	}

	public void setLibName(String libName) {
		this.libName = libName;
	}
	
	public String toString()
	{
		return String.format("Library Name: %s\n"
							+"Book Info:\n"
							+ bookInfo, libName);
	}
}
