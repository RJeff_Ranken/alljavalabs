package HOT3;

public class Math {

	public static int add2(int num1, int num2)
	{
		return num1 + num2;
	}
	
	public static int add3(int num1, int num2, int num3)
	{
		return num1 + num2 + num3;
	}
}
