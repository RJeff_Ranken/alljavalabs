package HOT3;

public class Book {

	private String name;
	private String publishDate;
	private String author;
	
	public Book (String n, String pd, String a)
	{
		this.name = n;
		this.publishDate = pd;
		this.author = a;
	}
	
	public String getName() {
		return name;
	}
	public String getPublishDate() {
		return publishDate;
	}
	public String getAuthor() {
		return author;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPublisDate(String publishDate) {
		this.publishDate = publishDate;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String toString()
	{
		return String.format("Name: %s\n"
							+"Publish Date: %s\n"
							+"Author: %s", getName(), getPublishDate(), getAuthor());
	}
}
