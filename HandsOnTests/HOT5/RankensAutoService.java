package HOT5;

import java.util.*;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.layout.GridPane;
import javafx.geometry.Pos;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class RankensAutoService extends Application{

	private CheckBox oil;
	private CheckBox lube;
	private CheckBox radFlush;
	private CheckBox transFlush;
	private CheckBox inspection;
	private CheckBox mufflerReplace;
	private CheckBox tireRotate;
	private TextField partCharge;
	private TextField laborHours;
	private Label total;
	private Button calcBtn;
	private Button exitProgram;
	private Label error;
	private double priceAcc = 0;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}
	
	public void start(Stage mainStage)
	{
		Label info1 = new Label("Routine Services");
		oil = new CheckBox("Oil Change ($26.00)");
		lube = new CheckBox("Lube job ($18.00)");
		radFlush = new CheckBox("Radiator Flush ($30.00)");
		transFlush = new CheckBox("Transmission Flush ($80.00)");
		inspection = new CheckBox("Inspection ($15.00)");
		mufflerReplace = new CheckBox("Muffler Replacement ($100.00)");
		tireRotate = new CheckBox("Tire Rotation ($20.00)");
		
		Label info2 = new Label("Nonroutine Services");
		Label chargeInstruct = new Label("Parts Charges: ");
		partCharge = new TextField();
		Label hourInstruct = new Label("Hours of Labor: ");
		laborHours = new TextField();
		error = new Label("");
		
		total = new Label("Total Charges: $0.00");
		
		calcBtn = new Button("Calculate Charges");
		calcBtn.setOnAction(new CalculateBtn());
		
		exitProgram = new Button("Exit");
		exitProgram.setOnAction(event ->
		{
			mainStage.close();
		});
		
		GridPane routine = new GridPane();
		GridPane nonRoutine = new GridPane();
		GridPane buttons = new GridPane();
		
		routine.add(info1, 0, 0);
		routine.add(oil, 0, 1);
		routine.add(lube, 0, 2);
		routine.add(radFlush, 0, 3);
		routine.add(transFlush, 0, 4);
		routine.add(inspection, 0, 5);
		routine.add(mufflerReplace, 0, 6);
		routine.add(tireRotate, 0, 7);
		
		routine.setVgap(10);
		
		nonRoutine.add(info2, 0, 0);
		nonRoutine.add(chargeInstruct, 0, 1);
		nonRoutine.add(partCharge, 1, 1);
		nonRoutine.add(hourInstruct, 0, 2);
		nonRoutine.add(laborHours, 1, 2);
		nonRoutine.add(error, 0, 3);
		
		buttons.add(total, 0, 0);
		buttons.add(calcBtn, 0, 1);
		buttons.add(exitProgram, 1, 1);
		
		buttons.setAlignment(Pos.CENTER);
		buttons.setHgap(5);
		
		VBox bigVert = new VBox(15, routine, nonRoutine, buttons);
		
		Scene scene1 = new Scene(bigVert, 260, 370);
		mainStage.setScene(scene1);
		mainStage.setTitle("Ranken's Automotive Maintenance");
		mainStage.show();
		
		
	}
	
	class CalculateBtn implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			double partChargeInput = Double.parseDouble(partCharge.getText());
			double hoursInput = Double.parseDouble(laborHours.getText());
			double laborCost;
			double totalCost;
			
			if (oil.isSelected())
				priceAcc += 26.00;
			
			if (lube.isSelected())
				priceAcc += 18.00;
			
			if (radFlush.isSelected())
				priceAcc += 30.00;
			
			if (transFlush.isSelected())
				priceAcc += 80.00;
			
			if (inspection.isSelected())
				priceAcc += 15.00;
			
			if (mufflerReplace.isSelected())
				priceAcc += 100.00;
			
			if (tireRotate.isSelected())
				priceAcc += 20.00;
			
			laborCost = hoursInput * 20;
			
			totalCost = priceAcc + laborCost + partChargeInput;
			
			total.setText(String.format("Total Charges: $%.2f", totalCost));
			
		}
	}
}
