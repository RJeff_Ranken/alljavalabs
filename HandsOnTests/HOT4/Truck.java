package HOT4;

public class Truck extends Vehicle {

	private int speed;
	 
	@Override
	public void accelerate()
	{
		System.out.println(speed += 3);
	}
}
