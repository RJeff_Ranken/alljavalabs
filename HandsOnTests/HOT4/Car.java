package HOT4;

public class Car extends Vehicle{
	
	private int speed;

	@Override
	public void accelerate()
	{
		System.out.println(speed += 10);
	}
}
