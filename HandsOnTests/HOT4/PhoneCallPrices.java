package HOT4;

public class PhoneCallPrices {

	private int[] areaCode = {262, 414, 608, 715, 815, 920};
	
	
	public PhoneCallPrices() 
	{
		
	}
	
	public int FindAreaAndRate(int areaInput)
	{
		int index, element;
		boolean found;
		
		index = 0;
		
		element = -1;
		found = false;
		
		while (!found && index < areaCode.length)
		{
			if(areaCode[index] == areaInput)
			{
				found = true;
				element = index;
			}
			
			index++;
		}
		return element;
	}
}
