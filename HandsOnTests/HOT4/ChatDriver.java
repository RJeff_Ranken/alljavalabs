package HOT4;

import java.util.Scanner;

public class ChatDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//input acceptors
		int areaInput; 
		int lengthInput;
		
		//arrays
		int[] areaCode = {262, 414, 608, 715, 815, 920};
		double[] perMinuteRate = {.07, .10, .05, .16, .24, .14};
		
		//counter
		int index = 0;
		
		//returns true or false
		boolean equal = true;
		Scanner keyboard = new Scanner(System.in);
		
		//asks for input
		System.out.print("Enter area code please: ");
		areaInput = keyboard.nextInt();
		
		System.out.print("Enter minutes used: ");
		lengthInput = keyboard.nextInt();
		double result = 0;
		for(int i=0;i<areaCode.length;++i)
		{
			if(areaCode[i] == areaInput)
			{
				result = perMinuteRate[i] * lengthInput;
			}
		}
		
		System.out.println(result);
		
		/*PhoneCallPrices call1 = new PhoneCallPrices();
	
		System.out.print(call1.FindAreaAndRate(areaInput));
		
		while(call1.FindAreaAndRate(areaInput))*/
		
		//ups the index if area code isnt found
		while(!equal && index < areaCode.length)
		{
			if(areaCode[index] == areaInput)
			{
				equal = true;
			}
				
			index++;
		}
		
		//extra confirmation for me to know its working
		if (areaCode[index] == areaInput)
		{
			System.out.println(areaCode[index]);
			System.out.println(perMinuteRate[index]);
			
		}
		else
			System.out.print("nope");
			
		//math to get total cost
		double math = lengthInput * perMinuteRate[index];
		
		//prints total cost
		System.out.printf("\n $%.2f", math);
		//System.out.print(perMinuteRate[index]);
	}

}
