package HOT4;

public class Vehicle {

	private int speed;

	public int getSpeed() {
		return speed;
	}

	public void accelerate()
	{
		System.out.println(speed += 5);
	}
}
