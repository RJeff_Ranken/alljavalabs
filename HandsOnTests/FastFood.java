import java.util.Scanner;

public class FastFood
{
	public static void main(String[] args)
	{
		//known variables                                                                 
		final double hamburgers = 1.25;
		final double cheeseburgers = 1.50;
		final double sodas = 1.95;
		final double fries = 0.95;
		
		//variables that will grab input from user
		int numberOfHamburgers;
		int numberOfCheeseburgers;
		int numberOfSodas;
		int numberOfFries;		
		String customerName;
		double total;
		
		Scanner keyboard = new Scanner(System.in);
		
		//asks user for QTY of hamburgers
		System.out.print("How many burgers would like?: ");
		numberOfHamburgers = keyboard.nextInt();
		keyboard.nextLine();
		
		//asks user for QTY of cheeseburgers
		System.out.print("How many cheeseburgers would you like?: ");
		numberOfCheeseburgers = keyboard.nextInt();
		keyboard.nextLine();
		
		//asks user for QTY of sodas
		System.out.print("How many sodas would you like?: ");
		numberOfSodas = keyboard.nextInt();
		keyboard.nextLine();
		
		//asks user for QTY of fries
		System.out.print("How many fries would you like?: ");
		numberOfFries = keyboard.nextInt();
		keyboard.nextLine();
		
		//asks user for name
		System.out.print("Name for order?: ");
		customerName = keyboard.nextLine();
		String upperName = customerName.toUpperCase();
		keyboard.nextLine();
		
		//calculates each food item with the QTY of each food item
		total = numberOfHamburgers * hamburgers + numberOfCheeseburgers * cheeseburgers
				+ numberOfSodas * sodas + numberOfFries * fries;
				
		//displays the receipt
		System.out.printf("%d hamburgers"
						+ "\n%d cheeseburgers"
						+ "\n%d sodas"
						+ "\n%d fries", numberOfHamburgers, numberOfCheeseburgers, numberOfSodas, numberOfFries);
		System.out.printf("\n$%,.2f Total" + " for " + upperName, total);
	}
}