package ex1;

public class Student {
	
	//Instance fields
	private String name;
	private int idNum;
	
	
	//Mutator
	//mutators help validate data input
	
	public void setName(String n)
	{
		name = n;
	}
	
	public void setId(int id)
	{
		idNum = id;
	}
	
	//Accessor
	public String getName()
	{
		return name;
	}
	
	public int getId()
	{
		return idNum;
	}
}
