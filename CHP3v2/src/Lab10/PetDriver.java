package Lab10;

import java.util.Scanner;

public class PetDriver {

	public static void main(String[] args) {
		String petName;
		String petType;
		int petAge;
		
		Scanner	keyboard = new Scanner(System.in);
		
		System.out.print("Enter the name of your pet: ");
		petName = keyboard.nextLine();
		
		System.out.print("What type of animal is it?: ");
		petType = keyboard.nextLine();
		
		System.out.print("How old is it?: ");
		petAge = keyboard.nextInt();
		
		Pet pet1 = new Pet(petName, petType, petAge);
		
		System.out.printf("Pet Info\n"
						+ "Pet Name: %s"
						+ "\nType of pet: %s"
						+ "\nPet's Age: %d", pet1.getName(), pet1.getType(), pet1.getAge());
	}

}
