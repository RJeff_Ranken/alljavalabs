package Lab3;

public class PersonalInfo {

	private String name;
	private String address;
	private int age;
	private String phone;
	
	public void setName(String n) {
		name = n;
	}
	public void setAddress(String a) {
		address = a;
	}
	public void setAge(int ag) {
		age = ag;
	}
	public void setPhone(String p) {
		phone = p;
	}
	
	public String getName() {
		return name;
	}
	public String getAddress() {
		return address;
	}
	public int getAge() {
		return age;
	}
	public String getPhone() {
		return phone;
	}
}
