package Lab3;

public class PersonalInfoDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		PersonalInfo info1 = new PersonalInfo();
		PersonalInfo info2 = new PersonalInfo();
		PersonalInfo info3 = new PersonalInfo();
		
		info1.setName("Richardo Jefferson");
		info1.setAddress("5047 N. Kingshighway");
		info1.setAge(27);
		info1.setPhone("314-494-8645");
		
		info2.setName("Melisa Naeger");
		info2.setAddress("3463 Grace Ave");
		info2.setAge(28);
		info2.setPhone("314-282-1589");
		
		info3.setName("Deborah Ward");
		info3.setAddress("5047 N. Kingshighway");
		info3.setAge(58);
		info3.setPhone("314-517-3757");
		
		System.out.printf("%s %s %d %s\n",info1.getName(),info1.getAddress(),info1.getAge(),info1.getPhone());
		System.out.printf("%s %19s %8d %s\n",info2.getName(),info2.getAddress(),info2.getAge(),info2.getPhone());
		System.out.printf("%s %26s %d %s",info3.getName(),info3.getAddress(),info3.getAge(),info3.getPhone());
	}

}
