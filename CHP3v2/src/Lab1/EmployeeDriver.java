package Lab1;

public class EmployeeDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Employee emp1 = new Employee();
		Employee emp2 = new Employee();
		Employee emp3 = new Employee();
		
		emp1.setName("Susan Myers");
		emp1.setId(47899);
		emp1.setDept("Accounting");
		emp1.setPos("Vice President");
		
		emp2.setName("Mark Jones");
		emp2.setId(39119);
		emp2.setDept("IT");
		emp2.setPos("Programmer");
		
		emp3.setName("Joy Rogers");
		emp3.setId(81774);
		emp3.setDept("Manufacturing");
		emp3.setPos("Engineer");
		
		System.out.println("Name         " + "ID     "+ "Department     " + "Position   ");
		System.out.print("----------------------------------------------------\n");
		System.out.printf("%s %d %12s %18s",emp1.getName(), emp1.getId(), emp1.getDept(), emp1.getPos());
		System.out.printf("\n%s %6d %4s %22s",emp2.getName(), emp2.getId(), emp2.getDept(), emp2.getPos());
		System.out.printf("\n%s %6d %15s %9s",emp3.getName(), emp3.getId(), emp3.getDept(), emp3.getPos());
	}

}
