package Lab2;

public class CarDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Car WRX = new Car(2018, "Subaru");
		
		System.out.print(WRX.getYearModel() + " " + WRX.getMake() + " " + WRX.getSpeed() + "mph");
		WRX.accelerate();
		System.out.print("\n" + WRX.addSpaces() + WRX.getSpeed() + "mph");
		WRX.accelerate();
		System.out.print("\n" + WRX.addSpaces() + WRX.getSpeed() + "mph");
		WRX.accelerate();
		System.out.print("\n" + WRX.addSpaces() + WRX.getSpeed() + "mph");
		WRX.accelerate();
		System.out.print("\n" + WRX.addSpaces() + WRX.getSpeed() + "mph");
		WRX.accelerate();
		System.out.print("\n" + WRX.addSpaces() + WRX.getSpeed() + "mph");
		
		System.out.print("\n\n" + WRX.getYearModel() + " " + WRX.getMake() + " " + WRX.getSpeed() + "mph");
		WRX.brake();
		System.out.print("\n" + WRX.addSpaces() + WRX.getSpeed() + "mph");
		WRX.brake();
		System.out.print("\n" + WRX.addSpaces() + WRX.getSpeed() + "mph");
		WRX.brake();
		System.out.print("\n" + WRX.addSpaces() + WRX.getSpeed() + "mph");
		WRX.brake();
		System.out.print("\n" + WRX.addSpaces() + WRX.getSpeed() + "mph");
		WRX.brake();
		System.out.print("\n" + WRX.addSpaces() + WRX.getSpeed() + "mph");
	}

}
