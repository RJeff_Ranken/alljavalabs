package Lab5;

public class RetailItem {

	private String description;
	private int unitsOnHand;
	private double price;
	
	public void setDesc(String d)
	{
		description = d;
	}
	
	public void setUnit(int u)
	{
		unitsOnHand = u;
	}
	
	public void setPrice(double p)
	{
		price = p;
	}
	
	public String getDesc()
	{
		return description;
	}
	
	public int getUnit()
	{
		return unitsOnHand;
	}
	
	public double getPrice()
	{
		return price;
	}
}
