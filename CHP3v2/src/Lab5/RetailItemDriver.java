package Lab5;

public class RetailItemDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		RetailItem Item1 = new RetailItem();
		RetailItem Item2 = new RetailItem();
		RetailItem Item3 = new RetailItem();
		
		Item1.setDesc("Item #1         Jacket");
		Item1.setUnit(12);
		Item1.setPrice(59.95);
		
		Item2.setDesc("Item #2         Designer Jeans");
		Item2.setUnit(40);
		Item2.setPrice(34.95);
		
		Item3.setDesc("Item #3         Shirt");
		Item3.setUnit(20);
		Item3.setPrice(24.95);
		
		System.out.print("Description                    " + " Units On Hand " + " Price\n");
		System.out.print("----------------------------------------------------\n");
		System.out.printf("%s %17d %11.2f\n",Item1.getDesc(),Item1.getUnit(),Item1.getPrice());
		System.out.printf("%s %9d %11.2f\n",Item2.getDesc(),Item2.getUnit(),Item2.getPrice());
		System.out.printf("%s %18d %11.2f",Item3.getDesc(),Item3.getUnit(),Item3.getPrice());
		
	}

}
