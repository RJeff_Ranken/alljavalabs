package Lab11;

public class PatientCharges {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Patient patient1 = new Patient("James", "Grey", "Chainland",
										"1020 N Lackely Blvd", "Chicago", "IL", 12345,
										"445-345-6789",
										"Gracedia Chainland", "445-908-8976");
		
		Procedure procedure1 = new Procedure("Physical Exam",
											 "1/22/2018",
											 "Dr. Irvine",
											 250.00);
		Procedure procedure2 = new Procedure("X-ray",
											 "1/22/2018",
											 "Dr. Jamison",
											 500.00);
		Procedure procedure3 = new Procedure("Blood test",
											 "1/22/2018",
											 "Dr. Smith",
											 200.00);
		
		double total = procedure1.getProcedureCharge() + procedure2.getProcedureCharge() + procedure3.getProcedureCharge();
		
		System.out.printf("Patient's name: %s %s %s"
						+ "\nArea info: %s, %s, %s %d"
						+ "\nPhone number: %s"
						+ "\nEmergency contact info: %s, %s", 
						patient1.getFirstName(), patient1.getMiddleName(), patient1.getLastName(),
						patient1.getAddress(), patient1.getCity(), patient1.getState(), patient1.getZip(),
						patient1.getNumber(),
						patient1.getEmergencyName(), patient1.getEmergencyNumber());
		
		System.out.printf("\n\n1st Procedure"
						+"\nProcedure: %s"
						+ "\nPrdocedure Date: %s"
						+ "\nPractitioner: %s"
						+ "\nCharge: %.2f", procedure1.getProcedureName(), procedure1.getProcedureDate(),
										procedure1.getPractitionerName(), procedure1.getProcedureCharge());
		
		System.out.printf("\n\n2nd Procedure"
				+"\nProcedure: %s"
				+ "\nPrdocedure Date: %s"
				+ "\nPractitioner: %s"
				+ "\nCharge: %.2f", procedure2.getProcedureName(), procedure2.getProcedureDate(),
								procedure2.getPractitionerName(), procedure2.getProcedureCharge());
		
		System.out.printf("\n\n3rd Procedure"
				+"\nProcedure: %s"
				+ "\nPrdocedure Date: %s"
				+ "\nPractitioner: %s"
				+ "\nCharge: %.2f", procedure3.getProcedureName(), procedure3.getProcedureDate(),
								procedure3.getPractitionerName(), procedure3.getProcedureCharge());
		
		System.out.printf("\n\nTotal charge for all three procedures: %.2f", total);
	}

}
