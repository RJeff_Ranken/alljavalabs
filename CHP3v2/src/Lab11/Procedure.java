package Lab11;

public class Procedure {

	private String procedureName;
	private String procedureDate;
	private String practitionerName;
	private double procedureCharge;
	
	public Procedure(String pn, String pd, String pract, double pc)
	{
		procedureName = pn;
		procedureDate = pd;
		practitionerName = pract;
		procedureCharge = pc;
	}

	public void setProcedureName(String pn) {
		procedureName = pn;
	}

	public void setProcedureDate(String pd) {
		procedureDate = pd;
	}

	public void setPractitionerName(String pract) {
		practitionerName = pract;
	}

	public void setProcedureCharge(double pc) {
		procedureCharge = pc;
	}
	
	public String getProcedureName() {
		return procedureName;
	}

	public String getProcedureDate() {
		return procedureDate;
	}

	public String getPractitionerName() {
		return practitionerName;
	}

	public double getProcedureCharge() {
		return procedureCharge;
	}

}
