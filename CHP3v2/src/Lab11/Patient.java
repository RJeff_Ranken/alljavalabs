package Lab11;

public class Patient {

	private String firstName;
	private String middleName;
	private String lastName;
	private String address;
	private String city;
	private String state;
	private int zip;
	private String number;
	private String emergencyName;
	private String emergencyNumber;
	
	public Patient(String fn, String mn, String ln, 
			String a, String c, String s, int z,
			String n,
			String eName, String eNumber)
	{
		firstName = fn;
		middleName = mn;
		lastName = ln;
		address = a;
		city = c;
		state = s;
		zip = z;
		number = n;
		emergencyName = eName;
		emergencyNumber = eNumber;
	}
	
	public void setFirstName(String fn) {
		firstName = fn;
	}
	public void setMiddleName(String mn) {
		middleName = mn;
	}
	public void setLastName(String ln) {
		lastName = ln;
	}
	public void setAddress(String a) {
		address = a;
	}
	public void setCity(String c) {
		city = c;
	}
	public void setState(String s) {
		state = s;
	}
	public void setZip(int z) {
		zip = z;
	}
	public void setNumber(String n) {
		number = n;
	}
	public void setEmergencyName(String eName) {
		emergencyName = eName;
	}
	public void setEmergencyNumber(String eNumber) {
		emergencyNumber = eNumber;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getAddress() {
		return address;
	}
	public String getCity() {
		return city;
	}
	public String getState() {
		return state;
	}
	public int getZip() {
		return zip;
	}
	public String getNumber() {
		return number;
	}
	public String getEmergencyName() {
		return emergencyName;
	}
	public String getEmergencyNumber() {
		return emergencyNumber;
	}
}
