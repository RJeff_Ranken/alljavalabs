package lab3;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.geometry.Pos;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class FoodCharges extends Application{

	private TextField priceInput;
	private Label percentTip;
	private Label salesTax;
	private Label totalCost;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		launch(args);
	}
	
	public void start(Stage mainStage)
	{
		Label priceInstruct = new Label("Enter the price for the food");
		percentTip = new Label();
		salesTax = new Label();
		totalCost = new Label();
		
		priceInput = new TextField();
		
		Button calcPrice = new Button("Calculate");
		calcPrice.setOnAction(new CalculateFood());
		
		VBox vbox1 = new VBox(percentTip, salesTax, totalCost);
		
		HBox hbox1 = new HBox(10, priceInstruct, priceInput);
		
		HBox hbox2 = new HBox(25, calcPrice, vbox1);
		
		VBox bigVert = new VBox(10, hbox1, hbox2);
		
		bigVert.setAlignment(Pos.CENTER);
		
		Scene scene1 = new Scene(bigVert);
		
		mainStage.setScene(scene1);
		
		mainStage.setTitle("Food Price");
		
		mainStage.show();
	}

	class CalculateFood implements EventHandler<ActionEvent>
	{
		@Override
		
		public void handle(ActionEvent Event)
		{
			double price = Double.parseDouble(priceInput.getText());
			
			double tip = price * 0.18;
			
			double tax = price * 0.07;
			
			double total = price + tip + tax;
			
			percentTip.setText(String.format("Tip: $%,.2f", tip));
			salesTax.setText(String.format("Tax: $%,.2f", tax));
			totalCost.setText(String.format("Total: $%,.2f", total));
		}
	}
}
