package lab4;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.geometry.Pos;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class PropertyTax extends Application{

	private TextField propValue;
	private Label assessment;
	private Label propTax;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}
	
	public void start(Stage mainStage)
	{
		Label propValInstruct = new Label("Enter the property value"
										+"\nto get the Assesment value and"
										+ "\nthe property tax");
		
		assessment = new Label();
		propTax = new Label();
		
		propValue = new TextField();
		
		Button calcValue = new Button("Calculate");
		calcValue.setOnAction(new ValueCalculator());
		
		VBox vbox1 = new VBox(assessment, propTax);
		
		HBox hbox1 = new HBox(10, propValInstruct, propValue);
		
		HBox hbox2 = new HBox(25, calcValue, vbox1);
		
		VBox bigVert = new VBox(10, hbox1, hbox2);
		
		bigVert.setAlignment(Pos.CENTER);
		
		Scene scene1 = new Scene(bigVert);
		
		mainStage.setScene(scene1);
		
		mainStage.setTitle("Tip, Tax, and Total");
		
		mainStage.show();
	}

	class ValueCalculator implements EventHandler<ActionEvent>
	{
		@Override
		
		public void handle(ActionEvent Event)
		{
			double value = Double.parseDouble(propValue.getText());
			
			double assessmentValue = value * 0.60;
			double dividedAssess = assessmentValue / 100;
			double propertyTax = dividedAssess * 0.64;
			
			assessment.setText(String.format("Assessment Value: $%,.2f", assessmentValue));
			propTax.setText(String.format("Property tax: $%,.2f", propertyTax));
		}
	}
}
