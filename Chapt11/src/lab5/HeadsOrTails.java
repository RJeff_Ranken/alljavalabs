package lab5;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.geometry.Pos;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

import java.util.Random;

public class HeadsOrTails extends Application {

	private Label flipResult;
	private Image HOrT;
	private ImageView hortImage;
	private Random randomNum;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	public void start(Stage mainStage)
	{
		Label clickInstruct = new Label("Click the button to "
								+ "\nget either heads (Lucario)"
								+ "\nor tails (Zoroark)");
		
		flipResult = new Label();
		
		HOrT = new Image("file:abstractSea.jpg");
		hortImage = new ImageView(HOrT);
		
		hortImage.setFitWidth(200);
		hortImage.setPreserveRatio(true);
		
		Button HeadsTails = new Button("Flip the Image!");
		HeadsTails.setOnAction(new FlipTheMon());
		
		HBox hbox1 = new HBox(clickInstruct, HeadsTails);
		
		VBox vbox1 = new VBox(hbox1, flipResult);
		
		VBox vbox2 = new VBox(hortImage);
		
		VBox bigVert = new VBox(vbox1, vbox2);
		
		Scene scene1 = new Scene(bigVert);
		
		mainStage.setScene(scene1);
		
		mainStage.setTitle("Heads or Tails!");
		
		mainStage.show();
	}
	
	class FlipTheMon implements EventHandler<ActionEvent>
	{
		@Override
		
		public void handle(ActionEvent Event)
		{
			randomNum = new Random();
			int ranFlip = randomNum.nextInt(2);
			
			if(ranFlip == 0)
			{
				HOrT = new Image("file:megaLucario.png");
				hortImage.setImage(HOrT);
				flipResult.setText("Heads!");
			}
			if(ranFlip == 1)
			{
				HOrT = new Image("file:Zoroark.png");
				hortImage.setImage(HOrT);
				flipResult.setText("Tails!");
			}
		}
	}
}
