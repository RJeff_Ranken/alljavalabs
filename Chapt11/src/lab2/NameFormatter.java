package lab2;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class NameFormatter extends Application{

	private Label lblResult1;
	
	private TextField fName;
	private TextField mName;
	private TextField lName;
	private TextField prefTitle;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage mainStage)  {
		// TODO Auto-generated method stub
		
		//buttons
		Button btn1 = new Button("Title, First, Middle, Last");
		btn1.setOnAction(new FirstFormatClickHandler());
		
		Button btn2 = new Button("First, Middle, Last");
		btn2.setOnAction(new SecondFormatClickHandler());
		
		Button btn3 = new Button("First and Last");
		btn3.setOnAction(new ThirdFormatClickHandler());
		
		Button btn4 = new Button("Last, First and Middle, Title");
		btn4.setOnAction(new FourthFormatClickHandler());
		
		Button btn5 = new Button("Last, First and Middle");
		btn5.setOnAction(new FifthFormatClickHandler());
		
		Button btn6 = new Button("Last, First");
		btn6.setOnAction(new SixthFormatClickHandler());
		
		//textfields
		fName = new TextField();
		mName = new TextField();
		lName = new TextField();
		prefTitle = new TextField();
		
		//instruct labels
		Label fNameInstruct = new Label("Enter your first name:");
		Label mNameInstruct = new Label("Enter your middle name:");
		Label lNameInstruct = new Label("Enter your last name:");
		Label titleInstruct = new Label("Enter your preffered title:");
		
		//labels that will display results
		lblResult1 = new Label();
		
		//hbox for input
		HBox hbox1 = new HBox(35, fNameInstruct, fName);
		HBox hbox2 = new HBox(20, mNameInstruct, mName);
		HBox hbox3 = new HBox(37, lNameInstruct, lName);
		HBox hbox4 = new HBox(15, titleInstruct, prefTitle);
		
		//vbox to align the 1st four hbox's
		VBox vbox1 = new VBox(hbox1, hbox2, hbox3, hbox4);
		
		
		//vbox to align all the buttons and the display label
		VBox vbox2 = new VBox(10, btn1, btn2, btn3, btn4, btn5, btn6, lblResult1);
		
		VBox bigVert = new VBox(10, vbox1, vbox2);
		
		bigVert.setAlignment(Pos.CENTER);
		
		Scene scene1 = new Scene(bigVert);
		
		mainStage.setScene(scene1);
		
		mainStage.setTitle("Name Formatter");
		
		mainStage.show();
	}

	class FirstFormatClickHandler implements EventHandler<ActionEvent>
	{
		@Override
			
		public void handle(ActionEvent Event)
		{	
			lblResult1.setText(prefTitle.getText() + " " + fName.getText() + " " +
							  mName.getText() + " " + lName.getText());
		}
	}
	
	class SecondFormatClickHandler implements EventHandler<ActionEvent>
	{
		@Override
			
		public void handle(ActionEvent Event)
		{	
			lblResult1.setText(fName.getText() + " " +
							  mName.getText() + " " + lName.getText());
		}
	}
	
	class ThirdFormatClickHandler implements EventHandler<ActionEvent>
	{
		@Override
			
		public void handle(ActionEvent Event)
		{	
			lblResult1.setText(fName.getText() + " " + lName.getText());
		}
	}
	
	class FourthFormatClickHandler implements EventHandler<ActionEvent>
	{
		@Override
			
		public void handle(ActionEvent Event)
		{	
			lblResult1.setText(lName.getText() + ", " +fName.getText() + " " + 
			mName.getText() + ", " + prefTitle.getText());
		}
	}
	
	class FifthFormatClickHandler implements EventHandler<ActionEvent>
	{
		@Override
			
		public void handle(ActionEvent Event)
		{	
			lblResult1.setText(lName.getText() + ", " +fName.getText() + " " + mName.getText());
		}
	}
	
	class SixthFormatClickHandler implements EventHandler<ActionEvent>
	{
		@Override
			
		public void handle(ActionEvent Event)
		{	
			lblResult1.setText(lName.getText() + ", " +fName.getText());
		}
	}
}