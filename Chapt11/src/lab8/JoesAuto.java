package lab8;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.layout.GridPane;
import javafx.geometry.Pos;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class JoesAuto extends Application{

	private CheckBox oilChange;
	private CheckBox lubeJob;
	private CheckBox radiationFlush;
	private CheckBox transmFlush;
	private CheckBox inspection;
	private CheckBox mufflerReplace;
	private CheckBox tireRotate;
	private Label laborInform;
	private Label laborInstruct;
	private TextField labor;
	private Label totalCost;
	private Button calcBtn;
	private double totalJob = 0;
	
	public static void main(String[] args)
	{
		launch(args);
	}
	
	public void start(Stage mainStage)
	{
		oilChange = new CheckBox("Oil Change: $35.00");
		lubeJob = new CheckBox("Lube Job: $25.00");
		radiationFlush = new CheckBox("Radiation Flush: $50.00");
		transmFlush = new CheckBox("Transmission Flush: $120.00");
		inspection = new CheckBox("Inspection: $35.00");
		mufflerReplace = new CheckBox("Muffler Replacement: $200.00");
		tireRotate = new CheckBox("Tire Rotation: $20.00");
		laborInform = new Label("Labor is $60 an hour");
		laborInstruct = new Label("Input the amount of hours you "
								+ "\nhave worked on the vehicle");
		labor = new TextField();
		totalCost = new Label();
		
		calcBtn = new Button("Calculate");
		calcBtn.setOnAction(new TotalButton());
		
		GridPane grid1 = new GridPane();
		GridPane grid2 = new GridPane();
		VBox bigVert;
		
		grid1.add(oilChange, 0, 0);
		grid1.add(lubeJob, 0, 1);
		grid1.add(radiationFlush, 0, 2);
		grid1.add(transmFlush, 0, 3);
		grid1.add(inspection, 0, 4);
		grid1.add(mufflerReplace, 0, 5);
		grid1.add(tireRotate, 0, 6);
		grid1.add(laborInform, 0, 7);
		grid1.add(laborInstruct, 0, 8);
		grid1.add(labor, 1, 8);
		
		grid2.add(calcBtn, 0, 0);
		grid2.add(totalCost, 1, 0);
		
		grid1.setVgap(10);
		
		bigVert = new VBox(grid1, grid2);
		
		Scene scene1 = new Scene(bigVert, 350, 300);
		mainStage.setScene(scene1);
		mainStage.setTitle("Joe's Automotive");
		mainStage.show();
	}
	
	class TotalButton implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent event) 
		{
			double hourIntake = Double.parseDouble(labor.getText());
			double laborInput;
			double workTotal;
			
			workTotal = 0;
			
			if (oilChange.isSelected())
				totalJob += 35.00;
			
			if (lubeJob.isSelected())
				totalJob += 25.00;
			
			if (radiationFlush.isSelected())
				totalJob += 50.00;
			
			if (transmFlush.isSelected())
				totalJob += 120.00;
			
			if (inspection.isSelected())
				totalJob += 35.00;
			
			if (mufflerReplace.isSelected())
				totalJob += 200.00;
			
			if (tireRotate.isSelected())
				totalJob += 20.00;
			
			laborInput = 60 * hourIntake;
			
			workTotal = totalJob + laborInput;
			
			totalCost.setText(String.format("The total for the work on this vehicle is: $%.2f", workTotal));
			
			//the code below was just experimental
			//for when the button is clicked, 
			//the checkboxes clear out
			
			/*if(oilChange.isSelected())
				oilChange.setSelected(false);
			
			if (lubeJob.isSelected())
				lubeJob.setSelected(false);
			
			if (radiationFlush.isSelected())
				radiationFlush.setSelected(false);
			
			if (transmFlush.isSelected())
				transmFlush.setSelected(false);
			
			if (inspection.isSelected())
				inspection.setSelected(false);
			
			if (mufflerReplace.isSelected())
				mufflerReplace.setSelected(false);
			
			if (tireRotate.isSelected())
				tireRotate.setSelected(false);*/
			
		}
		
	}
}
