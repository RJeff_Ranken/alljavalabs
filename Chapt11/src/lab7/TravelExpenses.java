package lab7;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.BorderPane;
import javafx.geometry.Pos;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class TravelExpenses extends Application{

	private TextField DayInput;
	private TextField AirFareFees;
	private TextField CarRentalFees;
	private TextField MileInput;
	private TextField ParkingFeeInput;
	private TextField TaxiChargesInput;
	private TextField RegistrationFeesInput;
	private TextField LodgingChargeInput;
	private Label TotalExpenses;
	private Label ExcessOwed;
	private Label AllowableExpenses;
	private Label ExtraCash;
	private Button calcBtn;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	public void start(Stage mainStage)
	{
		Label days = new Label("Number of days:");
		Label airfare = new Label("Amount of airfare:");
		Label carRental = new Label("Amount of car rental fees:");
		Label miles = new Label("Miles driven:");
		Label parkingFees = new Label("Amount of parking fees:");
		Label taxiCharges = new Label("Amount of taxi charges:");
		Label registrationFees = new Label("Registration Fees:");
		Label lodging = new Label("Lodging charges:");
		
		TotalExpenses = new Label("Total expenses");
		ExcessOwed = new Label("Amount owed");
		AllowableExpenses = new Label("Allowable Expenses");
		ExtraCash = new Label("Extra money to spend");
		
		DayInput = new TextField();
		AirFareFees = new TextField();
		CarRentalFees = new TextField();
		MileInput = new TextField();
		ParkingFeeInput = new TextField();
		TaxiChargesInput = new TextField();
		RegistrationFeesInput = new TextField();
		LodgingChargeInput = new TextField();
		
		calcBtn = new Button("Calculate");
		calcBtn.setOnAction(new TotalButton());
		
		GridPane expenseInput = new GridPane();
		GridPane expenseResults = new GridPane();
		BorderPane BPane1 = new BorderPane();
		VBox bigVert;
		HBox hbox1 = new HBox(calcBtn);
		
		hbox1.setAlignment(Pos.CENTER);
		BPane1.setBottom(hbox1);
		
		//Labels and TextFields for expense input
		expenseInput.add(days, 0, 0);
		expenseInput.add(DayInput, 1, 0);
		expenseInput.add(airfare, 0, 1);
		expenseInput.add(AirFareFees, 1, 1);
		expenseInput.add(carRental, 0, 2);
		expenseInput.add(CarRentalFees, 1, 2);
		expenseInput.add(miles, 0, 3);
		expenseInput.add(MileInput, 1, 3);
		expenseInput.add(parkingFees, 0, 4);
		expenseInput.add(ParkingFeeInput, 1, 4);
		expenseInput.add(taxiCharges, 0, 5);
		expenseInput.add(TaxiChargesInput, 1, 5);
		expenseInput.add(registrationFees, 0, 6);
		expenseInput.add(RegistrationFeesInput, 1, 6);
		expenseInput.add(lodging, 0, 7);
		expenseInput.add(LodgingChargeInput, 1, 7);
		
		//Labels for expense output
		expenseResults.add(TotalExpenses, 0, 0);
		expenseResults.add(ExcessOwed, 1, 0);
		expenseResults.add(AllowableExpenses, 0, 1);
		expenseResults.add(ExtraCash, 1, 1);
		
		expenseResults.setHgap(80);
		
		bigVert = new VBox(10, expenseInput, expenseResults, hbox1);
		
		Scene scene1 = new Scene(bigVert, 420, 300);
		mainStage.setScene(scene1);
		mainStage.setTitle("Travel Expenses");
		mainStage.show();
	}
	
	class TotalButton implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent Event)
		{
			double days = Double.parseDouble(DayInput.getText());
			double airfare = Double.parseDouble(AirFareFees.getText());
			double car = Double.parseDouble(CarRentalFees.getText());
			double miles = Double.parseDouble(MileInput.getText());
			double parkingFees = Double.parseDouble(ParkingFeeInput.getText());
			double taxi = Double.parseDouble(TaxiChargesInput.getText());
			double registerFees = Double.parseDouble(RegistrationFeesInput.getText());
			double lodge = Double.parseDouble(LodgingChargeInput.getText());
			
			double mealPrice = 47, parkFees = 20.00, taxiCharge = 40.00, 
					lodgingFee = 195.00, privateVehicle = 0.42;
			
			double mileConversion;
			
			double totalIncome = (mealPrice + parkFees + taxiCharge + lodgingFee) * days;
			
			double moneySpentPerDay = 0;
			
			double multipleDays;
			
			double amountOwed = 0;
			double cashToSpend= 0;
			
			if(airfare > 0)
			{
				moneySpentPerDay += airfare;
			}
			if(car > 0)
			{
				moneySpentPerDay += car;
			}
			if(parkingFees > 0)
			{
				moneySpentPerDay += parkingFees;
			}
			if(taxi > 0)
			{
				moneySpentPerDay += taxi;
			}
			if(registerFees > 0)
			{
				moneySpentPerDay += registerFees;
			}
			if(lodge > 0)
			{
				moneySpentPerDay += lodge;
			}
			if(miles > 0)
			{
				mileConversion = miles * privateVehicle;
				totalIncome += mileConversion;
			}
			
			multipleDays = moneySpentPerDay * days;
			
			if(multipleDays > totalIncome)
			{
				amountOwed = multipleDays - totalIncome;
				cashToSpend = 0;
			}
			if(totalIncome > multipleDays)
			{
				cashToSpend = totalIncome - multipleDays;
				amountOwed = 0;
			}
			
			TotalExpenses.setText(String.format("Total expenses: $%.2f", multipleDays));
			ExcessOwed.setText(String.format("Amount owed: $%.2f", amountOwed));
			AllowableExpenses.setText(String.format("Allowable Expenses: $%.2f", totalIncome));
			ExtraCash.setText(String.format("Extra money to spend: $%.2f", cashToSpend));
		}
	}
}
