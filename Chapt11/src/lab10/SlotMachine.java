package lab10;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

import java.util.Random;

public class SlotMachine extends Application{

	private Label winOnSpin;
	private Label totalWinnings;
	private TextField betInput;
	
	private Image wheel1;
	private ImageView wheelImage1;
	
	private Image wheel2;
	private ImageView wheelImage2;
	
	private Image wheel3;
	private ImageView wheelImage3;
	
	private Random ranWheels = new Random();
	private double winningAcc = 0;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	public void start(Stage mainStage)
	{
		//labels and inputs
		Label moneylbl = new Label("Amount Inserted: $");
		winOnSpin = new Label("Amount Won This Spin: $0.00");
		totalWinnings = new Label("Total Amount Won: $0.00");
		betInput = new TextField();
		
		//wheels
		//using the oranges as the opening display
		//I didn't want to leave it out
		wheel1 = new Image("file:orange.png");
		wheelImage1 = new ImageView(wheel1);
		wheelImage1.setFitWidth(200);
		wheelImage1.setPreserveRatio(true);
		
		wheel2 = new Image("file:orange.png");
		wheelImage2 = new ImageView(wheel2);
		wheelImage2.setFitWidth(200);
		wheelImage2.setPreserveRatio(true);
		
		wheel3 = new Image("file:orange.png");
		wheelImage3 = new ImageView(wheel3);
		wheelImage3.setFitWidth(200);
		wheelImage3.setPreserveRatio(true);
		
		//button
		Button slotSpin = new Button("SPIN!");
		slotSpin.setOnAction(new SpinSlotWheels());
		
		//setting the display
		BorderPane BPane1 = new BorderPane();
		BorderPane BPane2 = new BorderPane();
		GridPane wheels = new GridPane();
		
		HBox hbox1 = new HBox(slotSpin);
		
		hbox1.setAlignment(Pos.CENTER);
		BPane2.setBottom(hbox1);
		
		wheels.setGridLinesVisible(true);
		wheels.setHgap(20);
		wheels.setVgap(20);
		wheels.setPadding(new Insets(20));
		
		wheels.add(wheelImage1, 0, 0);
		wheels.add(wheelImage2, 1, 0);
		wheels.add(wheelImage3, 2, 0);
		
		/*HBox wheels = new HBox(10, wheelImage1, wheelImage2, wheelImage3);
		wheels.setPadding(new Insets(30));*/
		
		HBox betSet = new HBox(moneylbl, betInput);
		
		VBox winningsInfo = new VBox(winOnSpin, totalWinnings); 
		
		BPane1.setLeft(betSet);
		BPane1.setRight(winningsInfo);
		
		
		VBox vbox1 = new VBox(wheels, BPane1);
		//VBox vbox2 = new VBox(slotSpin);
		VBox bigVert = new VBox(vbox1, hbox1);
		Scene scene1 = new Scene(bigVert);
		
		mainStage.setScene(scene1);
		
		mainStage.setTitle("Slot Machine");
		
		mainStage.show();
	}
	
	class SpinSlotWheels implements EventHandler<ActionEvent>
	{
		@Override
		
		public void handle(ActionEvent Event)
		{
			//ranWheels = new Random();
			int firstWheel = ranWheels.nextInt(7);
			int secondWheel = ranWheels.nextInt(7);
			int thirdWheel = ranWheels.nextInt(7);
			double betIntake = Double.parseDouble(betInput.getText());
			double winDouble = betIntake * 2;
			double winTriple = betIntake * 3;
			double nada = 0;
			//double winningAcc = 0;
			
			/****************
			 * FIRST WHEEL
			 ****************/
			if(firstWheel == 0)
			{
				wheel1 = new Image("file:raspberry.png");
				wheelImage1.setImage(wheel1);
				wheelImage1.setFitWidth(200);
				wheelImage1.setFitHeight(200);
				wheelImage1.setPreserveRatio(true);
			}
			if(firstWheel == 1)
			{
				wheel1 = new Image("file:strawberry.png");
				wheelImage1.setImage(wheel1);
				wheelImage1.setFitWidth(200);
				wheelImage1.setFitHeight(200);
				wheelImage1.setPreserveRatio(true);
			}
			if(firstWheel == 2)
			{
				wheel1 = new Image("file:cherry.png");
				wheelImage1.setImage(wheel1);
				wheelImage1.setFitWidth(200);
				wheelImage1.setFitHeight(200);
				wheelImage1.setPreserveRatio(true);
			}
			if(firstWheel == 3)
			{
				wheel1 = new Image("file:orange.png");
				wheelImage1.setImage(wheel1);
				wheelImage1.setFitWidth(200);
				wheelImage1.setFitHeight(200);
				wheelImage1.setPreserveRatio(true);
			}
			if(firstWheel == 4)
			{
				wheel1 = new Image("file:watermelon.png");
				wheelImage1.setImage(wheel1);
				wheelImage1.setFitWidth(200);
				wheelImage1.setFitHeight(200);
				wheelImage1.setPreserveRatio(true);
			}
			if(firstWheel == 5)
			{
				wheel1 = new Image("file:lemon.png");
				wheelImage1.setImage(wheel1);
				wheelImage1.setFitWidth(200);
				wheelImage1.setFitHeight(200);
				wheelImage1.setPreserveRatio(true);
			}
			if(firstWheel == 6)
			{
				wheel1 = new Image("file:apple.png");
				wheelImage1.setImage(wheel1);
				wheelImage1.setFitWidth(200);
				wheelImage1.setFitHeight(200);
				wheelImage1.setPreserveRatio(true);
			}
			
			/****************
			 * SECOND WHEEL
			 ****************/
			if(secondWheel == 0)
			{
				wheel2 = new Image("file:raspberry.png");
				wheelImage2.setImage(wheel2);
				wheelImage2.setFitWidth(200);
				wheelImage2.setFitHeight(200);
				wheelImage2.setPreserveRatio(true);
			}
			if(secondWheel == 1)
			{
				wheel2 = new Image("file:strawberry.png");
				wheelImage2.setImage(wheel2);
				wheelImage2.setFitWidth(200);
				wheelImage2.setFitHeight(200);
				wheelImage2.setPreserveRatio(true);
			}
			if(secondWheel == 2)
			{
				wheel2 = new Image("file:cherry.png");
				wheelImage2.setImage(wheel2);
				wheelImage2.setFitWidth(200);
				wheelImage2.setFitHeight(200);
				wheelImage2.setPreserveRatio(true);
			}
			if(secondWheel == 3)
			{
				wheel2 = new Image("file:orange.png");
				wheelImage2.setImage(wheel2);
				wheelImage2.setFitWidth(200);
				wheelImage2.setFitHeight(200);
				wheelImage2.setPreserveRatio(true);
			}
			if(secondWheel == 4)
			{
				wheel2 = new Image("file:watermelon.png");
				wheelImage2.setImage(wheel2);
				wheelImage2.setFitWidth(200);
				wheelImage2.setFitHeight(200);
				wheelImage2.setPreserveRatio(true);
			}
			if(secondWheel == 5)
			{
				wheel2 = new Image("file:lemon.png");
				wheelImage2.setImage(wheel2);
				wheelImage2.setFitWidth(200);
				wheelImage2.setFitHeight(200);
				wheelImage2.setPreserveRatio(true);
			}
			if(secondWheel == 6)
			{
				wheel2 = new Image("file:apple.png");
				wheelImage2.setImage(wheel2);
				wheelImage2.setFitWidth(200);
				wheelImage2.setFitHeight(200);
				wheelImage2.setPreserveRatio(true);
			}
			
			/****************
			 * THIRD WHEEL
			 ****************/
			if(thirdWheel == 0)
			{
				wheel3 = new Image("file:raspberry.png");
				wheelImage3.setImage(wheel3);
				wheelImage3.setFitWidth(200);
				wheelImage3.setFitHeight(200);
				wheelImage3.setPreserveRatio(true);
			}
			if(thirdWheel == 1)
			{
				wheel3 = new Image("file:strawberry.png");
				wheelImage3.setImage(wheel3);
				wheelImage3.setFitWidth(200);
				wheelImage3.setFitHeight(200);
				wheelImage3.setPreserveRatio(true);
			}
			if(thirdWheel == 2)
			{
				wheel3 = new Image("file:cherry.png");
				wheelImage3.setImage(wheel3);
				wheelImage3.setFitWidth(200);
				wheelImage1.setFitHeight(200);
				wheelImage3.setPreserveRatio(true);
			}
			if(thirdWheel == 3)
			{
				wheel3 = new Image("file:orange.png");
				wheelImage3.setImage(wheel3);
				wheelImage3.setFitWidth(200);
				wheelImage3.setFitHeight(200);
				wheelImage3.setPreserveRatio(true);
			}
			if(thirdWheel == 4)
			{
				wheel3 = new Image("file:watermelon.png");
				wheelImage3.setImage(wheel3);
				wheelImage3.setFitWidth(200);
				wheelImage3.setFitHeight(200);
				wheelImage3.setPreserveRatio(true);
			}
			if(thirdWheel == 5)
			{
				wheel3 = new Image("file:lemon.png");
				wheelImage3.setImage(wheel3);
				wheelImage3.setFitWidth(200);
				wheelImage3.setFitHeight(200);
				wheelImage3.setPreserveRatio(true);
			}
			if(thirdWheel == 6)
			{
				wheel3 = new Image("file:apple.png");
				wheelImage3.setImage(wheel3);
				wheelImage3.setFitWidth(200);
				wheelImage3.setFitHeight(200);
				wheelImage3.setPreserveRatio(true);
			}
			
			if(firstWheel == secondWheel && secondWheel == thirdWheel && thirdWheel == firstWheel)
			{
				winOnSpin.setText(String.format("Amount Won This Spin: %.2f", winTriple));
				winningAcc += winTriple;
			}
			else if(firstWheel == secondWheel || secondWheel == thirdWheel || thirdWheel == firstWheel)
			{
				winOnSpin.setText(String.format("Amount Won This Spin: %.2f", winDouble));
				
				winningAcc += winDouble;
			}
			
			else
			{
				winOnSpin.setText(String.format("Amount Won This Spin: %.2f", nada));
				winningAcc -= betIntake;
			}
			
			totalWinnings.setText(String.format("Total Amount Won: %.2f", winningAcc));
		}
	}
}
