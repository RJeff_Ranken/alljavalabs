package lab1;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class LatinTranslator extends Application{

	private Label lbl1;
	private Label lbl2;
	private Label lbl3;
	
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		launch(args);
	}
	

	@Override
	public void start(Stage mainStage) throws Exception {
		// TODO Auto-generated method stub
		
		Button btn1 = new Button("Click this button to translate \nSinister from Latin to English");
		btn1.setOnAction(new LeftClickHandler());
		
		Button btn2 = new Button("Click this button to translate \nDexter from Latin to English");
		btn2.setOnAction(new RightClickHandler());
		
		Button btn3 = new Button("Click this button to translate \nMedium from Latin to English");
		btn3.setOnAction(new CenterClickHandler());
		
		lbl1 = new Label("");
		lbl2 = new Label("");
		lbl3 = new Label("");
		
		
		GridPane grid1 = new GridPane();
		
		grid1.add(btn1, 0, 0);
		grid1.add(lbl1, 1, 0);
		grid1.add(btn2, 0, 1);
		grid1.add(lbl2, 1, 1);
		grid1.add(btn3, 0, 2);
		grid1.add(lbl3, 1, 2);
		
		Scene scene1 = new Scene(grid1, 300, 150); 
		
		mainStage.setScene(scene1);
		
		mainStage.setTitle("Latin Translator");
		
		mainStage.show();
	}
	
	class LeftClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			lbl1.setText("This means left!");
		}
	}

	class RightClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			lbl2.setText("This means right!");
		}
	}
	
	class CenterClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			lbl3.setText("This means center!");
		}
	}
}