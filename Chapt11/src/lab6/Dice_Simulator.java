package lab6;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.geometry.Pos;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

import java.util.Random;

public class Dice_Simulator extends Application{

	private Label numResult1;
	private Label numResult2;
	private Image die1;
	private ImageView diceSide1;
	private Image die2;
	private ImageView diceSide2;
	private Random dieCast;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	public void start(Stage mainStage)
	{
		Label clickInstruct = new Label("Click the button to roll a pair of dice!");
		
		numResult1 = new Label();
		numResult2 = new Label();
		
		die1 = new Image("file:naturerocks.jpg");
		diceSide1 = new ImageView(die1);
		diceSide1.setFitWidth(200);
		diceSide1.setPreserveRatio(true);
		
		die2 = new Image("file:waterripples.jpg");
		diceSide2 = new ImageView(die2);
		diceSide2.setFitWidth(200);
		diceSide2.setPreserveRatio(true);
		
		Button diceRoll = new Button("Roll the dice!");
		diceRoll.setOnAction(new DiceRollin());
		
		HBox hbox1 = new HBox(20, diceSide1, diceSide2);
		HBox hbox2 = new HBox(250, numResult1, numResult2);
		VBox vbox1 = new VBox(15, clickInstruct, diceRoll, hbox1, hbox2);
		
		Scene scene1 = new Scene(vbox1);
		
		mainStage.setScene(scene1);
		
		mainStage.setTitle("Time to roll some dice!");
		
		mainStage.show();
	}
	
	class DiceRollin implements EventHandler<ActionEvent>
	{
		@Override
		
		public void handle(ActionEvent Event)
		{
			dieCast = new Random();
			int ranRoll1 = dieCast.nextInt(6);
			int ranRoll2 = dieCast.nextInt(6);
			
			
			//rolling for die 1
			if(ranRoll1 == 0)
			{
				die1 = new Image("file:dice1.png");
				diceSide1.setImage(die1);
				numResult1.setText("1");
			}
			if(ranRoll1 == 1)
			{
				die1 = new Image("file:dice2.png");
				diceSide1.setImage(die1);
				numResult1.setText("2");
			}
			if(ranRoll1 == 2)
			{
				die1 = new Image("file:dice3.png");
				diceSide1.setImage(die1);
				numResult1.setText("3");
			}
			if(ranRoll1 == 3)
			{
				die1 = new Image("file:dice4.png");
				diceSide1.setImage(die1);
				numResult1.setText("4");
			}
			if(ranRoll1 == 4)
			{
				die1 = new Image("file:dice5.png");
				diceSide1.setImage(die1);
				numResult1.setText("5");
			}
			if(ranRoll1 == 5)
			{
				die1 = new Image("file:dice6.png");
				diceSide1.setImage(die1);
				numResult1.setText("6");
			}
			
			//rolling for die 2
			if(ranRoll2 == 0)
			{
				die2 = new Image("file:dice1.png");
				diceSide2.setImage(die2);
				numResult2.setText("1");
			}
			if(ranRoll2 == 1)
			{
				die2 = new Image("file:dice2.png");
				diceSide2.setImage(die2);
				numResult2.setText("2");
			}
			if(ranRoll2 == 2)
			{
				die2 = new Image("file:dice3.png");
				diceSide2.setImage(die2);
				numResult2.setText("3");
			}
			if(ranRoll2 == 3)
			{
				die2 = new Image("file:dice4.png");
				diceSide2.setImage(die2);
				numResult2.setText("4");
			}
			if(ranRoll2 == 4)
			{
				die2 = new Image("file:dice5.png");
				diceSide2.setImage(die2);
				numResult2.setText("5");
			}
			if(ranRoll2 == 5)
			{
				die2 = new Image("file:dice6.png");
				diceSide2.setImage(die2);
				numResult2.setText("6");
			}
		}
	}
}
