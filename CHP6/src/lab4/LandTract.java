package lab4;

public class LandTract {

	private int length;
	private int width;
	private int land1;
	private int land2;
	private Object land1String;
	
	public void setLength(int l) {
		length = l;
	}
	public void setWidth(int w) {
		width = w;
	}
	public int getLength() {
		return length;
	}
	public int getWidth() {
		return width;
	}
	
	public int getLand1() {
		return land1;
	}
	public int getLand2() {
		return land2;
	}
	public void setLand1(int l1) {
		land1 = l1;
	}
	public void setLand2(int l2) {
		land2 = l2;
	}
	public int getArea()
	{
		int area = length * width;
		
		return area;
	}
	
	public boolean isEquals(int l1, int l2)
	{
		boolean t;
		land1 = l1;
		land2 = l2;
		land1String = String.format("%d",land1);
		String land2String = String.format("%d", land2);
		String equalOrNot;
		
		if(this.land1String.equals(land2String))
		{
			 t = true;
		}
		else
		{
			t = false; 
		}
		
		return t;
	}
	
	public String toString(int l1, int l2)
	{
		land1 = l1;
		land2 = l2;
		land1String = String.format("%d",land1);
		String land2String = String.format("%d", land2);
		String equalOrNot;
		
		if(this.land1String.equals(land2String))
		{
			// t = true;
			 equalOrNot = "Both of these lands are equal";
		}
		else
		{
			 //t = false;
			 equalOrNot = "These two lands are not equal";
		}
		
		
		return equalOrNot;
		
	}
}
