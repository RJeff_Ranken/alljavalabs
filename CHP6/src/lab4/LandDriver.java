package lab4;

import java.util.Scanner;

public class LandDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("What is the length of the first land: ");
		int land1L = keyboard.nextInt();
		
		System.out.print("What is the width of the first land: ");
		int land1W = keyboard.nextInt();
		
		System.out.print("What is the length of the second land: ");
		int land2L = keyboard.nextInt();
		
		System.out.print("What is the width of the second land: ");
		int land2W = keyboard.nextInt();
		
		LandTract land1 = new LandTract();
		land1.setLength(land1L);
		land1.setWidth(land1W);
		System.out.printf("Field 1: %d",land1.getArea());
		land1.setLand1(land1.getArea());
		
		LandTract land2 = new LandTract();
		land2.setLength(land2L);
		land2.setWidth(land2W);
		System.out.printf("\nField 2: %d",land2.getArea());
		land2.setLand2(land2.getArea());
		
		LandTract bothLands = new LandTract();
		System.out.print("\n");
		System.out.print(bothLands.isEquals(land1.getArea(), land2.getArea()));
		System.out.print("\n");
		System.out.print(bothLands.toString(land1.getArea(), land2.getArea()));
	}

}
