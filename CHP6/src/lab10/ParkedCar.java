package lab10;

public class ParkedCar {

	private int carMake;
	private String model;
	private String color;
	private String licenseNum;
	private int minutesParked;
	private ParkingMeter minPurchased;
	
	public int getCarMake() {
		return carMake;
	}
	public String getModel() {
		return model;
	}
	public String getColor() {
		return color;
	}
	public String getLicenseNum() {
		return licenseNum;
	}
	public int getMinutesParked() {
		return minutesParked;
	}
	public void setCarMake(int carMake) {
		this.carMake = carMake;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public void setLicenseNum(String licenseNum) {
		this.licenseNum = licenseNum;
	}
	public void setMinutesParked(int minutesParked) {
		this.minutesParked = minutesParked;
	}
	
	public ParkedCar(int make, String mod, String c,
					String lisc, int mPark, int minPurch)
	{
		carMake = make;
		model = mod;
		color = c;
		licenseNum = lisc;
		minutesParked = mPark;
		minPurchased = new ParkingMeter(minPurch);
	}
	
	public class ParkingMeter {
		
		public int minutesPurchased;
		
		public ParkingMeter(int mp)
		{
			minutesPurchased = mp;
		}
	}
}
