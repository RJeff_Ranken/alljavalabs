package lab10;

public class ParkingMeter {

	private int minutesPurchase;

	public int getMinutesPurchase() {
		return minutesPurchase;
	}

	public void setMinutesPurchase(int minutesPurchase) {
		this.minutesPurchase = minutesPurchase;
	}
	
}
