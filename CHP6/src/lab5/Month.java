package lab5;

public class Month {

	private int monthNumber;
	
	public Month()
	{
		monthNumber = 1;
	}
	
	public Month(int num)
	{
		monthNumber = num;
		
		if(monthNumber < 1 || monthNumber > 12)
		{
			monthNumber = 1;
		}
	}
	
	public Month(String m)
	{
		String monthName = m;
		
		if(monthName.equalsIgnoreCase("January"))
		{
			monthNumber = 1;
		}
		else if(monthName.equalsIgnoreCase("February"))
		{
			monthNumber = 2;
		}
		else if(monthName.equalsIgnoreCase("March"))
		{
			monthNumber = 3;
		}
		else if(monthName.equalsIgnoreCase("April"))
		{
			monthNumber = 4;
		}
		else if(monthName.equalsIgnoreCase("May"))
		{
			monthNumber = 5;
		}
		else if(monthName.equalsIgnoreCase("June"))
		{
			monthNumber = 6;
		}
		else if(monthName.equalsIgnoreCase("July"))
		{
			monthNumber = 7;
		}
		else if(monthName.equalsIgnoreCase("August"))
		{
			monthNumber = 8;
		}
		else if(monthName.equalsIgnoreCase("September"))
		{
			monthNumber = 9;
		}
		else if(monthName.equalsIgnoreCase("October"))
		{
			monthNumber = 10;
		}
		else if(monthName.equalsIgnoreCase("November"))
		{
			monthNumber = 11;
		}
		else if(monthName.equalsIgnoreCase("December"))
		{
			monthNumber = 12;
		}
	}
	
	public int setMonthNumber(int m)
	{
		
		
		
		return monthNumber;
	}
	
	public int getMonthNumber()
	{
		
	}
	
}
