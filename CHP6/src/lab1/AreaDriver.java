package lab1;

import java.util.Scanner;

public class AreaDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double radInput;
		double wInput;
		double lInput;
		double hInput;
		
		Scanner keyboard = new Scanner(System.in);
		
		//gets the area of a circle
		System.out.print("Enter a number to get the radius of a circle: ");
		radInput = keyboard.nextDouble();
		double circle = Area.getArea(radInput);
		System.out.printf("The area of this circle is %.1f", circle);
		
		//gets the area of a rectangle
		System.out.print("\n\nEnter a number for the width: ");
		wInput = keyboard.nextDouble();
		System.out.print("Now enter a number for the length: ");
		lInput = keyboard.nextDouble();
		double rectangle = Area.getArea(wInput, lInput);
		System.out.printf("The area of this rectangle is %.1f", rectangle);
		
		//gets the area of a cylinder
		System.out.print("\n\nEnter a number for the radius: ");
		radInput = keyboard.nextDouble();
		System.out.print("Now enter a number for the height: ");
		hInput = keyboard.nextDouble();
		double cylinder = Area.getArea(radInput, hInput, Math.PI);
		System.out.printf("The area of this cylinder is %.1f", cylinder);
	}

}
