package lab1;

public class Area {

	public static double getArea(double radius)
	{
		double area;
		area = Math.PI * radius * radius;
		
		return area;
	}
	
	public static double getArea(double width, double length)
	{
		double area;
		area = width * length;
		
		return area;
	}
	
	public static double getArea(double radius, double height, double pi)
	{
		double area;
		area = Math.PI * radius * radius * height;
		
		return area;
	}
}
