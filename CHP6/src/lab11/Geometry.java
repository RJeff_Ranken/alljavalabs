package lab11;

public class Geometry {

	public static double circleArea(double radius)
	{
		return Math.PI * radius * radius;
	}
	
	public static double rectArea(double width, double length)
	{
		return width * length;
	}
	
	public static double triArea(double base, double height)
	{
		return base * height * 0.5;
	}
}
