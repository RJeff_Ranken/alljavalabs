package lab11;

import java.util.Scanner;

public class GeoCalc {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		double radius, width, length, base, height;
		double placeThere;
		int selectionInput;

		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Welcome to this geometry calculator"
						+"\nSelect one of the three options:"
						+"\npress 1 for circle"
						+"\npress 2 for rectangle"
						+"\npress 3 for triangle"
						+"\nor press 4 to quit ");
		selectionInput = keyboard.nextInt();
		while(selectionInput != 4)
		{
			if(selectionInput == 1)
			{
				System.out.print("Enter a number for the radius: ");
				radius = keyboard.nextDouble();
				placeThere = Geometry.circleArea(radius);
				System.out.printf("The area of this circle is %.1f\n", placeThere);
			}
			if(selectionInput == 2)
			{
				System.out.print("Enter a number for width: ");
				width = keyboard.nextDouble();
				System.out.print("Enter a number for length: ");
				length = keyboard.nextDouble();
				placeThere = Geometry.rectArea(width, length);
				System.out.printf("The area of this rectangle is %.1f", placeThere);
			}
			if(selectionInput == 3)
			{
				System.out.print("Enter a number for the base: ");
				base = keyboard.nextDouble();
				System.out.print("Enter a number for height: ");
				height = keyboard.nextDouble();
				placeThere = Geometry.triArea(base, height);
				System.out.printf("The area of this triangle is %.1f", placeThere);
			}
			System.out.print("\nSelect one of the three options:"
					+"\npress 1 for circle"
					+"\npress 2 for rectangle"
					+"\npress 3 for triangle"
					+"\nor press 4 to quit ");
			selectionInput = keyboard.nextInt();
		}
	}
}
