package lab2;

import java.util.Scanner;

public class ItemDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		InventoryItem inv1 = new InventoryItem("Strawberry Pocky", 200);
		
		InventoryItem inv2 = new InventoryItem(inv1);
		
		System.out.printf("%s %d", inv1.getDescription(), inv1.getUnits());
		System.out.printf("\n%s %d", inv2.getDescription(), inv2.getUnits());
	}

}
