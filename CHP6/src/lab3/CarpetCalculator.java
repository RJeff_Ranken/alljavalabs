package lab3;

import java.util.Scanner;

public class CarpetCalculator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double lInput;
		double wInput;
		double cCost;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter the length of the floor: ");
		lInput = keyboard.nextDouble();
		System.out.print("Enter the width of the floor: ");
		wInput = keyboard.nextDouble();
		
		RoomDimension part1 = new RoomDimension(lInput, wInput);
		
		double area = part1.getArea();
		
		String roomy = part1.toString();
		System.out.printf(roomy);
		
		System.out.print("\nEnter the cost of the carpet per SqFt: ");
		cCost = keyboard.nextDouble();
		
		RoomCarpet part2 = new RoomCarpet(part1, cCost);
		
		double area2 = part2.getArea();
		
		String roomy2 = part2.toString();
		
		System.out.print(roomy2);
	}

}
