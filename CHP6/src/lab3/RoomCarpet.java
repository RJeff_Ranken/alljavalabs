package lab3;

public class RoomCarpet {

	private RoomDimension size;
	private double carpetCost;
	
	public RoomCarpet(RoomDimension dim, double cost)
	{
		size = dim;
		carpetCost = cost;
	}
	
	public RoomDimension getSize() {
		return size;
	}
	public double getCarpetCost() {
		return carpetCost;
	}
	public void setSize(RoomDimension size) {
		this.size = size;
	}
	public void setCarpetCost(double carpetCost) {
		this.carpetCost = carpetCost;
	}
	
	public double getArea()
	{
		double costPerSqFt = size.getLength() * size.getWidth() * carpetCost;
		
		return costPerSqFt;
	}
	
	public String toString()
	{
		return String.format("The cost would be $%.2f", getArea());
	}
}
