package lab3;

public class RoomDimension {

	private double length;	
	private double width;
	
	public RoomDimension(double len, double w)
	{
		length = len;
		width = w;
	}
	
	public double getLength() {
		return length;
	}
	public double getWidth() {
		return width;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	
	public double getArea()
	{
		double area = length * width;
		return area;
	}
	
	public String toString()
	{
		return String.format("The total area is %.0f", getArea());
	}
}
