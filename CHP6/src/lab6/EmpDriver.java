package lab6;

public class EmpDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Employee emp1 = new Employee("Susan Myers", 47899, "Accounting", "Vice President");
		Employee emp2 = new Employee("Mark Jones", 39119);
		Employee emp3 = new Employee();
		
		System.out.println("Name         " + "ID     "+ "Department     " + "Position   ");
		System.out.print("----------------------------------------------------\n");
		System.out.printf("%s %d %12s %18s",emp1.getName(), emp1.getId(), emp1.getDept(), emp1.getPos());
		System.out.printf("\n%s %6d %4s %22s",emp2.getName(), emp2.getId(), emp2.getDept(), emp2.getPos());
		System.out.printf("\n%s %14d %15s %9s",emp3.getName(), emp3.getId(), emp3.getDept(), emp3.getPos());
	}

}
