package lab6;

public class Employee {
	
	private String name;
	private int idNumber;
	private String department;
	private String position;
	
	public Employee(String name, int idNumber, String department, String position) {
		this.name = name;
		this.idNumber = idNumber;
		this.department = department;
		this.position = position;
	}
	
	public Employee(String name, int idNumber)
	{
		this.name = name;
		this.idNumber = idNumber;
		this.department = "";
		this.position = "";
	}
	
	public Employee()
	{
		this.name = "";
		this.idNumber = 0;
		this.department = "";
		this.position = "";
	}
	
	public void setName(String n)
	{
		name = n;
	}
	
	public void setId(int id)
	{
		idNumber = id;
	}
	
	public void setDept(String d)
	{
		department = d;
	}
	
	public void setPos(String p)
	{
		position = p;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getId()
	{
		return idNumber;
	}
	
	public String getDept()
	{
		return department;
	}
	
	public String getPos()
	{
		return position;
	}
}
