package lab12;

public class FuelGauge {

	public final int MAX_GALLONS = 15;
	private int gallons; 
	
	public FuelGauge()
	{
		
	}
	
	public FuelGauge(int g)
	{
		gallons = g;
	}

	public int getGallons() {
		return gallons;
	}
	
	public void incrementGallons()
	{
		gallons += 1;
	}
	
	public void decrementGallons()
	{
		gallons -= 1;
	}
}
