package lab12;

public class Odometer {

	public final int MAX_MILEAGE = 999999;
	public final int MPG = 24;
	private int mileage;
	private int setPoint;
	private FuelGauge fuelGauge;
	
	public Odometer(int m, FuelGauge fg)
	{
		mileage = m;
		fuelGauge = fg;
	}

	public int getMileage() {
		return mileage;
	}
	
	public void incrementMileage()
	{
		mileage += 1;
	}
}
