package lab12;

import java.util.Scanner;

public class CarSimulator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		FuelGauge carPart1 = new FuelGauge(0);
		
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("What is the milage on the car? ");
		int mileInput = keyboard.nextInt();
		Odometer carPart2 = new Odometer(mileInput, carPart1);
		System.out.print("Enter gallon amount: ");
		int galInput = keyboard.nextInt();
		
		while(galInput > carPart1.MAX_GALLONS || galInput < 0)
		{
			System.out.print("Enter a valid amount from 0 to 15: ");
		
			galInput = keyboard.nextInt();
		}
		System.out.print("\nFilling the car with gas!\n");
		for(carPart1.getGallons(); carPart1.getGallons() < galInput;)
		{
			carPart1.incrementGallons();
			System.out.printf("1 gallon pumped\n\n");
		}
		
		System.out.printf(carPart1.getGallons() + " gallon(s) of gas has been filled");
		
		System.out.print("\n\n");
		
		for(int z = 1; z <= carPart1.getGallons(); z++)
		{			
			for(int y = 1; y <= carPart2.MPG; y++)
			{   
				carPart2.incrementMileage();
				System.out.printf("\n%d mile(s) driven on %d gallon(s) left.", carPart2.getMileage(), z);
			}
		}
	}
}
